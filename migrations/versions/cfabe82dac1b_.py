"""empty message

Revision ID: cfabe82dac1b
Revises: f42dacd6dac1
Create Date: 2020-11-29 16:13:10.917224

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.exc import OperationalError

# revision identifiers, used by Alembic.
revision = "cfabe82dac1b"
down_revision = "f42dacd6dac1"
branch_labels = None
depends_on = None


def upgrade():
    try:
        op.add_column("user", sa.Column("design", sa.String(length=40)))
    except OperationalError:
        pass


def downgrade():
    try:
        op.drop_column("user", "design")
    except OperationalError:
        pass
