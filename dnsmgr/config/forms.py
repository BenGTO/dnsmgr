"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField, BooleanField
from wtforms.validators import DataRequired, Length, IPAddress


class EditForm(FlaskForm):
    domain_name = StringField(
        "Domain name", validators=[DataRequired(), Length(min=1, max=255)]
    )
    domain_name_server = StringField(
        "Domain name server", validators=[DataRequired(), Length(min=1, max=1024)]
    )
    netbios_name_server = StringField(
        "Netbios name server", validators=[DataRequired(), Length(min=1, max=1024)]
    )
    default_netmask = StringField(
        "Default netmask",
        validators=[DataRequired(), IPAddress(), Length(min=1, max=255)],
    )
    default_lease_time = IntegerField("Default lease time", validators=[DataRequired()])
    max_lease_time = IntegerField("Maximum lease time", validators=[DataRequired()])
    shared_network = BooleanField("Shared networks")
    master_dns = StringField(
        "Primary DNS name", validators=[DataRequired(), Length(min=1, max=255)]
    )
    dns_ttl = IntegerField("Time to live", validators=[DataRequired()])
    webhook = StringField("Webhook")
    submit = SubmitField("Save globals")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.config = kwargs["obj"]
