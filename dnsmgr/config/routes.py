"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import render_template, request, url_for, flash
from flask_login import login_required, current_user
from . import config_blueprint
from .forms import EditForm
from dnsmgr.models import Config
from dnsmgr import db


@config_blueprint.route("/config", methods=["GET", "POST"])
@login_required
def index():
    context = {}
    context["breadcrumbs"] = [{"target": url_for("network.index"), "label": "Networks"}]

    configs = Config.query.all()
    if len(configs) > 0:
        context["config"] = configs[0]
    else:
        context["config"] = Config()
        db.session.add(context["config"])
        db.session.commit()

    context["form"] = EditForm(obj=context["config"])

    if request.method == "POST":
        if current_user.role == "readonly":
            flash("You only have read access", "error")
        elif context["form"].validate_on_submit():
            context["form"].populate_obj(context["config"])
            db.session.commit()
            flash("Settings saved", "success")

    return render_template("config/config.html", context=context)  # pragma: no cover
