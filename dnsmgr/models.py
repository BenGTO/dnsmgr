"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import current_app
from flask_login import UserMixin
from dnsmgr import db, bcrypt
from dnsmgr.cryptography.auth_token import AuthToken
from datetime import datetime


class Alias(db.Model):
    __tablename__ = "alias"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    comment = db.Column(db.String(4096))
    host_id = db.Column(db.Integer, db.ForeignKey("host.id"), nullable=False)

    def __repr__(self):
        return "<Alias %r>" % self.name  # pragma: no cover


class Host(db.Model):
    __tablename__ = "host"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    address = db.Column(db.String(255), unique=True, nullable=False)
    network_id = db.Column(db.Integer, db.ForeignKey("network.id"), nullable=False)
    mac = db.Column(db.String(25), unique=True, nullable=True)
    comment = db.Column(db.String(4096))
    # hack...
    addr_sorting = db.Column(db.BigInteger, unique=True, nullable=False)

    aliases = db.relationship("Alias", backref="host", cascade="all, delete-orphan")

    def __repr__(self):
        return "<Host %r>" % self.name  # pragma: no cover

    # def checkAddress


class Network(db.Model):
    __tablename__ = "network"
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(255), unique=True, nullable=False)
    netmask = db.Column(db.String(20), nullable=False)
    gateway = db.Column(db.String(255), unique=True, nullable=False)
    name = db.Column(db.String(255), unique=True, nullable=False)
    broadcast = db.Column(db.String(255), unique=True, nullable=False)
    dhcp_pool_start = db.Column(db.String(255), nullable=False, default="")
    dhcp_pool_end = db.Column(db.String(255), nullable=False, default="")
    dhcp_pool_known_clients = db.Column(db.Boolean, nullable=False, default=False)
    dhcp_pool_unknown_clients = db.Column(db.Boolean, nullable=False, default=True)
    dhcp_dns_server = db.Column(db.String(1020), nullable=False)
    dhcp_netbios_server = db.Column(db.String(255))
    dhcp_default_lease_time = db.Column(db.Integer, default=7200)
    dhcp_max_lease_time = db.Column(db.Integer, default=27200)
    dhcp_min_secs = db.Column(db.Integer)
    dhcp_nextserver = db.Column(db.String(255), default="")
    dhcp_filename = db.Column(db.String(255), default="")

    # hack...
    addr_sorting = db.Column(db.BigInteger, unique=True, nullable=False)

    hosts = db.relationship("Host", backref="network", cascade="all, delete-orphan")

    def __repr__(self):
        return "<Network %r>" % self.name  # pragma: no cover


class Config(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    domain_name = db.Column(db.String(255), nullable=False)
    domain_name_server = db.Column(db.String(2048), nullable=False)
    netbios_name_server = db.Column(db.String(2048), nullable=False)
    default_netmask = db.Column(db.String(255), nullable=False)
    default_lease_time = db.Column(db.Integer, nullable=False)
    max_lease_time = db.Column(db.Integer, nullable=False)
    shared_network = db.Column(db.Boolean, nullable=False)
    master_dns = db.Column(db.String(255), nullable=False)
    dns_ttl = db.Column(db.Integer, nullable=False)
    webhook = db.Column(db.String(512))

    def __init__(self):
        self.domain_name = ""
        self.domain_name_server = ""
        self.netbios_name_server = ""
        self.default_netmask = ""
        self.default_lease_time = 1200
        self.max_lease_time = 86400
        self.shared_network = False
        self.master_dns = ""
        self.dns_ttl = 86400
        self.webhook = ""


class Token(db.Model):
    """
    Access token owned by user
    """

    __tablename__ = "token"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.VARCHAR(64), nullable=False)
    user_id = db.Column(db.Integer, nullable=False)
    token = db.Column(db.VARCHAR(1000), nullable=False)
    creation_date = db.Column(db.TIMESTAMP, nullable=False)
    expiration_date = db.Column(db.TIMESTAMP, nullable=False)
    description = db.Column(db.VARCHAR(256))

    def __init__(self, expiration_sec, user_id, data, description, name):
        token = AuthToken(
            current_app.config["SECRET_KEY"], expiration_sec, user_id, data
        )
        self.token = token.token
        self.user_id = token.user_id
        self.creation_date = datetime.now()
        self.expiration_date = token.expiry_date(self.creation_date)
        self.name = name
        self.description = description

    def __eq__(self, other):
        return self.token == other.token

    def __str__(self):
        return self.name

    def get_data(self):
        return AuthToken(self.token).data()

    def has_expired(self):
        return self.expiration_date >= datetime.now()

    # Jinja custom filter did not work with pytest
    def creation_date_fmt(self):
        return self.creation_date.strftime("%Y-%m-%d %H:%M:%S")

    def expiration_date_fmt(self):
        return self.expiration_date.strftime("%Y-%m-%d %H:%M:%S")


class User(db.Model, UserMixin):
    """
    Class that represents a user of the application
    """

    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    login = db.Column(db.String(40), unique=True, nullable=False)
    hashed_password = db.Column(db.LargeBinary(60), nullable=False)
    authenticated = db.Column(db.Boolean, default=False)
    registered_on = db.Column(db.DateTime, nullable=True)
    role = db.Column(db.String(40), default="user")
    design = db.Column(db.String(40), default="default")

    # tokens = db.relationship("AuthToken", backref="user", cascade="all, delete-orphan")

    def __init__(
        self, login="", plaintext_password="", role="user", design="default"
    ):  # nosec B107
        self.login = login
        if len(plaintext_password) > 0:
            self.hashed_password = bcrypt.generate_password_hash(plaintext_password)
        self.authenticated = False
        self.registered_on = datetime.now()
        self.role = role
        self.design = design

    def set_password(self, plaintext_password):
        self.hashed_password = bcrypt.generate_password_hash(plaintext_password)

    def is_correct_password(self, plaintext_password):
        return bcrypt.check_password_hash(self.hashed_password, plaintext_password)

    def __repr__(self):
        return "<User %r>" % self.name  # pragma: no cover
