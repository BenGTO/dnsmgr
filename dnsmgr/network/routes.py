"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import render_template, request, redirect, url_for, flash, current_app, abort
from flask_login import login_required, current_user
from . import network_blueprint
from .forms import AddForm, EditForm, DeleteForm
from dnsmgr.models import Network, Config, Host
from dnsmgr.user.roles import readonly_denied
from dnsmgr import db
from datetime import datetime
from ipaddress import ip_network


@network_blueprint.route("/network")
@login_required
def index():
    context = {}
    breadcrumbs = [{"target": url_for("network.index"), "label": "Networks"}]
    context["breadcrumbs"] = breadcrumbs

    context["networks"] = Network.query.order_by(Network.addr_sorting).all()
    return render_template("network/index.html", context=context)


@network_blueprint.route("/network/add", methods=["GET", "POST"])
@login_required
@readonly_denied
def add():
    context = {
        "breadcrumbs": [{"target": url_for("network.index"), "label": "Networks"}],
        "form": AddForm(),
    }

    if request.method == "POST":
        if context["form"].validate_on_submit():
            network = Network()
            context["form"].populate_obj(network)

            (q1, q2, q3, q4) = network.address.split(".")
            network.addr_sorting = (
                int(q4) + int(q3) * 256 + int(q2) * 65536 + int(q1) * 16777216
            )

            net = ip_network(
                network.address
                + "/"
                + str(sum([bin(int(x)).count("1") for x in network.netmask.split(".")]))
            )
            network.broadcast = str(net.broadcast_address)

            try:  # this should never fail after form validation
                db.session.add(network)
                db.session.commit()
            except Exception:  # pragma: no cover
                flash("Could not add network (duplicate?)", "error")  # pragma: no cover
                return render_template(
                    "network/edit.html", context=context
                )  # pragma: no cover

            return redirect(url_for("network.index"))
    return render_template("network/edit.html", context=context)


@network_blueprint.route(
    "/network/edit/<string:network_address>", methods=["GET", "POST"]
)
@login_required
def edit(network_address):
    context = {
        "breadcrumbs": [{"target": url_for("network.index"), "label": "Networks"}],
        "network": Network.query.filter_by(address=network_address).first(),
    }

    context["form"] = EditForm(obj=context["network"])
    if request.method == "POST":
        if current_user.role == "readonly":
            flash("You only have read access", "error")
        elif context["form"].validate_on_submit():
            context["form"].populate_obj(context["network"])
            try:  # this should never fail after form validation
                db.session.commit()
            except Exception:  # pragma: no cover
                flash("Could not update network", "error")  # pragma: no cover
                return render_template(
                    "network/edit.html", context=context
                )  # pragma: no cover

            return redirect(url_for("network.index"))
    if request.method == "GET":
        if context["network"] is None:
            flash("Network not found")
            return render_template("network/index.html", context=context)

    return render_template("network/edit.html", context=context)


@network_blueprint.route(
    "/network/delete/<string:network_address>", methods=["GET", "POST"]
)
@login_required
@readonly_denied
def delete(network_address):
    context = {}
    context["breadcrumbs"] = [{"target": url_for("network.index"), "label": "Networks"}]

    context["network"] = Network.query.filter_by(address=network_address).first()
    context["form"] = DeleteForm(obj=context["network"])
    if request.method == "POST":
        if context["network"]:
            db.session.delete(context["network"])
            db.session.commit()

        return redirect(url_for("network.index"))

    return render_template("network/delete.html", context=context)


@network_blueprint.route("/network/dhcpd.conf")
def dhcpd_conf():
    if (
        current_app.config["RESTRICT_ACCESS_TO_DUMPS"]
        and not current_user.is_authenticated
    ):
        abort(403)
    context = {}
    context["networks"] = Network.query.order_by(Network.addr_sorting).all()
    configs = context["config"] = Config.query.all()
    if len(configs) == 0 or configs[0].domain_name == "":
        return redirect(url_for("config.index"))

    context["config"] = configs[0]

    return render_template("dhcpd.conf.j2", context=context)  # pragma: no cover


@network_blueprint.route("/network/db.site")
def dns_conf():
    if (
        current_app.config["RESTRICT_ACCESS_TO_DUMPS"]
        and not current_user.is_authenticated
    ):
        abort(403)
    context = {}
    context["hosts"] = Host.query.order_by(Host.addr_sorting).all()
    context["config"] = Config.query.all()[0]

    dt = datetime.now()
    context["serial"] = dt.strftime("%Y%m%d%M")

    return render_template("db.site.j2", context=context)  # pragma: no cover


@network_blueprint.route("/network/db.rev/<string:network_address>")
def dns_rev_conf(network_address):
    if (
        current_app.config["RESTRICT_ACCESS_TO_DUMPS"]
        and not current_user.is_authenticated
    ):
        abort(403)
    context = {}
    context["network"] = Network.query.filter_by(address=network_address).first()
    context["config"] = Config.query.all()[0]

    dt = datetime.now()
    context["serial"] = dt.strftime("%Y%m%d%M")

    return render_template("db.rev.j2", context=context)  # pragma: no cover
