"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, BooleanField, IntegerField
from wtforms.validators import DataRequired, Length, IPAddress, Optional
from dnsmgr.models import Network
from ipaddress import ip_network, ip_address
from dnsmgr.helpers.string_checks import contains_whitespace


class EditForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired(), Length(min=2, max=255)])
    address = StringField(
        "Address",
        validators=[
            DataRequired(),
            IPAddress(ipv6=True, message="Invalid address"),
            Length(min=7, max=255),
        ],
    )
    netmask = StringField(
        "Netmask",
        validators=[
            DataRequired(),
            IPAddress(ipv6=True, message="Invalid address"),
            Length(min=7, max=255),
        ],
    )
    gateway = StringField(
        "Gateway",
        validators=[
            DataRequired(),
            IPAddress(ipv6=True, message="Invalid address"),
            Length(min=7, max=255),
        ],
    )
    default_network = BooleanField("Default network")
    dhcp_pool_start = StringField(
        "Start",
        validators=[IPAddress(ipv6=True, message="Invalid address"), Optional()],
    )
    dhcp_pool_end = StringField(
        "End", validators=[IPAddress(ipv6=True, message="Invalid address"), Optional()]
    )
    dhcp_pool_known_clients = BooleanField("Accept known clients")
    dhcp_pool_unknown_clients = BooleanField("Accept unknown clients")
    dhcp_dns_server = StringField(
        "Name servers", validators=[DataRequired(), Length(min=2, max=255)]
    )
    dhcp_netbios_server = StringField(
        "Netbios server", validators=[Optional(), Length(min=0, max=255)]
    )
    dhcp_default_lease_time = IntegerField(
        "Default lease time", validators=[DataRequired()], default=1800
    )
    dhcp_max_lease_time = IntegerField(
        "Maximum lease time", validators=[DataRequired()], default=14400
    )
    dhcp_min_secs = IntegerField("Offering delay", validators=[Optional()])
    dhcp_filename = StringField(
        "Name of PXE boot file", validators=[Optional(), Length(min=0, max=255)]
    )
    dhcp_nextserver = StringField(
        "PXE Bootserver, iPXE URL etc.", validators=[Optional(), Length(min=0, max=255)]
    )

    submit = SubmitField("Save network")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Edit Network"

    def validate(self, extra_validators=None):
        rv = FlaskForm.validate(self)
        if not rv:
            return False

        if contains_whitespace(self.name.data):
            self.name.errors.append("Whitespace detected")
            return False

        try:
            net = ip_network(
                self.address.data
                + "/"
                + str(
                    sum([bin(int(x)).count("1") for x in self.netmask.data.split(".")])
                )
            )
        except ValueError:  # pragma: no cover
            self.address.errors.append("Invalid address list")  # pragma: no cover
            return False  # pragma: no cover

        if ip_address(self.gateway.data) not in list(net.hosts()):
            self.gateway.errors.append("Gateway outside network range")
            return False

        if self.dhcp_pool_start.data is None:
            self.dhcp_pool_start.data = ""
        if self.dhcp_pool_start.data != "" and ip_address(
            self.dhcp_pool_start.data
        ) not in list(net.hosts()):
            self.dhcp_pool_start.errors.append("Range start outside network range")
            return False

        if self.dhcp_pool_end.data is None:
            self.dhcp_pool_end.data = ""
        if self.dhcp_pool_end.data != "" and ip_address(
            self.dhcp_pool_end.data
        ) not in list(net.hosts()):
            self.dhcp_pool_end.errors.append("Range end outside network range")
            return False

        if self.dhcp_pool_start.data != "" and self.dhcp_pool_end.data != "":
            if ip_address(self.dhcp_pool_start.data) >= ip_address(
                self.dhcp_pool_end.data
            ):
                self.dhcp_pool_end.errors.append(
                    "End of range must be higher than start"
                )
                return False

        if self.dhcp_pool_start.data == "" and self.dhcp_pool_end.data != "":
            self.dhcp_pool_start.errors.append("Start mandatory if end is set")
            return False
        if self.dhcp_pool_start.data != "" and self.dhcp_pool_end.data == "":
            self.dhcp_pool_end.errors.append("End mandatory if start is set")
            return False

        for server in self.dhcp_dns_server.data.split(","):
            try:
                ip_network(server, True)
            except ValueError:
                self.dhcp_dns_server.errors.append("Invalid address")
                return False
        if not (
            self.dhcp_netbios_server.data is None or self.dhcp_netbios_server.data == ""
        ):
            for server in self.dhcp_netbios_server.data.split(","):
                try:
                    ip_network(server, True)
                except ValueError:
                    self.dhcp_netbios_server.errors.append("Invalid address")
                    return False
        else:
            self.dhcp_netbios_server.data = ""

        if self.dhcp_max_lease_time.data <= self.dhcp_default_lease_time.data:
            self.dhcp_max_lease_time.errors.append("Must be longer than default value")
            return False

        return True


class AddForm(EditForm):
    submit = SubmitField("Add network")

    def __init__(self, *args, **kwargs):
        EditForm.__init__(self, *args, **kwargs)
        self.title = "Add a Network"
        self.network = None

    def validate(self, extra_validators=None):
        rv = EditForm.validate(self)
        if not rv:
            return False

        # Test for duplicates
        network = Network.query.filter_by(name=self.name.data).first()
        if network is not None:
            if network.name == self.name.data:
                self.name.errors.append("Duplicate name")
                return False
        network = Network.query.filter_by(address=self.address.data).first()
        if network is not None:
            if network.address == self.address.data:
                self.address.errors.append("Duplicate address")
                return False
        return True


class DeleteForm(FlaskForm):
    address = HiddenField()
    name = HiddenField()
    submit = SubmitField("Delete network")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Delete Network"
