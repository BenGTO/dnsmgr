"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import render_template, request, redirect, url_for, flash
from flask_login import login_required, current_user
from . import host_blueprint
from .forms import AddForm, EditForm, DeleteForm
from dnsmgr.models import Host, Network
from dnsmgr.user.roles import readonly_denied
from dnsmgr import db


@host_blueprint.route("/host/add/<string:network_address>", methods=["GET", "POST"])
@login_required
@readonly_denied
def add(network_address):
    context = {
        "breadcrumbs": [{"target": url_for("network.index"), "label": "Networks"}],
        "network": Network.query.filter_by(address=network_address).first(),
    }

    host = Host()
    host.network_id = context["network"].id
    context["form"] = AddForm(obj=host)

    if request.method == "POST" and context["form"].validate_on_submit():
        context["form"].populate_obj(host)
        (q1, q2, q3, q4) = host.address.split(".")
        host.addr_sorting = (
            int(q4) + int(q3) * 256 + int(q2) * 65536 + int(q1) * 16777216
        )
        # Edit form has 'id', but will be auto assigned when adding hosts
        host.id = None
        db.session.add(host)
        try:
            db.session.commit()
        except Exception:  # pragma: no cover
            # Should never get here after form validation
            flash("Could not add host (duplicate?)", "error")  # pragma: no cover

        return redirect(
            url_for("host.host_list", network_address=context["network"].address)
        )
    return render_template("host/edit.html", context=context)


@host_blueprint.route("/host/edit/<string:host_address>", methods=["GET", "POST"])
@login_required
def edit(host_address):
    context = {}
    host = Host.query.filter_by(address=host_address).first()
    context["breadcrumbs"] = [
        {"target": url_for("network.index"), "label": "Networks"},
        {
            "target": url_for("host.host_list", network_address=host.network.address),
            "label": "Host list",
        },
    ]

    context["form"] = EditForm(obj=host)
    context["host"] = host

    if request.method == "POST" and context["form"].validate_on_submit():
        if current_user.role == "readonly":
            flash("You only have read access", "error")
        else:
            (q1, q2, q3, q4) = host.address.split(".")
            host.addr_sorting = (
                int(q4) + int(q3) * 256 + int(q2) * 65536 + int(q1) * 16777216
            )
            context["form"].populate_obj(host)
            try:
                db.session.commit()
            except Exception:  # pragma: no cover
                # Should never get here after form validation
                flash("Could not update network", "error")  # pragma: no cover
                return render_template(
                    "host/edit.html", context=context
                )  # pragma: no cover

        return redirect(url_for("host.host_list", network_address=host.network.address))

    return render_template("host/edit.html", context=context)


@host_blueprint.route("/host/list/<string:network_address>", methods=["GET", "POST"])
@login_required
def host_list(network_address):
    context = {}
    context["network"] = Network.query.filter_by(address=network_address).first()
    context["hosts"] = Host.query.filter_by(network=context["network"]).order_by(
        Host.addr_sorting
    )
    context["breadcrumbs"] = [{"target": url_for("network.index"), "label": "Networks"}]

    return render_template("host/hostlist.html", context=context)


@host_blueprint.route("/host/delete/<string:host_address>", methods=["GET", "POST"])
@login_required
@readonly_denied
def delete(host_address):
    context = {}
    context["host"] = Host.query.filter_by(address=host_address).first()
    try:
        context["network"] = context["host"].network
    except AttributeError:
        flash("Attempted to delete a non existent host", "error")
        return render_template("network/index.html", context=context)

    context["breadcrumbs"] = [
        {"target": url_for("network.index"), "label": "Networks"},
        {
            "target": url_for(
                "host.host_list", network_address=context["host"].network.address
            ),
            "label": "Host list",
        },
    ]

    context["form"] = DeleteForm(obj=context["host"])
    if request.method == "POST":
        db.session.delete(context["host"])
        db.session.commit()

        return redirect(
            url_for("host.host_list", network_address=context["host"].network.address)
        )

    return render_template("host/delete.html", context=context)  # pragma: no cover
