"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, TextAreaField
from wtforms.validators import DataRequired, Length, IPAddress
from dnsmgr.models import Host, Network, Alias
from ipaddress import ip_network, ip_address
from dnsmgr.helpers.string_checks import is_valid_dns_name
import re


class EditForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired(), Length(min=1, max=255)])
    address = StringField(
        "Address",
        validators=[
            DataRequired(),
            IPAddress(ipv6=True, message="Invalid address"),
            Length(min=6, max=255),
        ],
    )
    mac = StringField("MAC", validators=[Length(max=20)])
    network_id = HiddenField()
    id = HiddenField()
    comment = TextAreaField("Comment")
    submit = SubmitField("Save host")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Edit Host"

    def validate(self, extra_validators=None):
        rv = FlaskForm.validate(self)
        if not rv:  # pragma: no cover
            return False  # pragma: no cover

        if not is_valid_dns_name(self.name.data):
            self.name.errors.append("Invalid character(s) detected")
            return False

        network = Network.query.filter_by(id=self.network_id.data).first()
        network_ip = ip_network(
            network.address
            + "/"
            + str(sum([bin(int(x)).count("1") for x in network.netmask.split(".")]))
        )
        if ip_address(self.address.data) not in list(network_ip.hosts()):
            self.address.errors.append("Address not in subnet range")
            return False

        # name already defined?
        host = Host.query.filter_by(name=self.name.data).first()
        if host is not None:
            # if no host id: new host, duplicate. If host id matches, no duplicate
            if (
                self.id.data is None
                or self.id.data == ""
                or int(host.id) != int(self.id.data)
            ):
                self.name.errors.append("Duplicate name")
                return False
        alias = Alias.query.filter_by(name=self.name.data).first()
        if alias is not None:
            self.name.errors.append("Duplicate name")
            return False

        # address already defined?
        host = Host.query.filter_by(address=self.address.data).first()
        if host is not None:
            if (
                self.id.data is None
                or self.id.data == ""
                or int(host.id) != int(self.id.data)
            ):
                self.address.errors.append("Duplicate address")
                return False

        if self.mac.data == "":
            self.mac.data = None

        if self.mac.data is not None:  # pragma: no cover
            host = Host.query.filter_by(mac=self.mac.data).first()
            if host is not None:
                if self.id.data == "" or str(host.id) != str(self.id.data):
                    self.mac.errors.append("Duplicate MAC")
                    return False

        return True


class AddForm(EditForm):
    submit = SubmitField("Add host")

    def __init__(self, *args, **kwargs):
        EditForm.__init__(self, *args, **kwargs)
        self.title = "Add a Host"
        self.host = None

    def validate(self, extra_validators=None):
        rv = EditForm.validate(self)
        if not rv:
            return False

        if self.mac.data is not None and len(self.mac.data) > 0:
            if not re.match(
                "[0-9a-f]{2}(:?)[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", self.mac.data.lower()
            ):
                self.mac.errors.append("Not a valid MAC address")
                return False

        return True


class DeleteForm(FlaskForm):
    address = HiddenField()
    name = HiddenField()
    submit = SubmitField("Delete host")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Delete Host"
