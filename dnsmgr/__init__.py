"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import os
import dnsmgr.appconfig as appconfig
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy as SA
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bcrypt import Bcrypt


# Manage disconnects (https://github.com/mitsuhiko/flask-sqlalchemy/issues/589)
class SQLAlchemy(SA):
    def apply_pool_defaults(self, app, options):
        SA.apply_pool_defaults(self, app, options)
        options["pool_pre_ping"] = True


try:
    f = open(".version.txt")
    version = f.readline()
except FileNotFoundError:  # pragma: no cover
    version = "unset"  # pragma: no cover

db = SQLAlchemy()
bcrypt = Bcrypt()


def create_app(config_filename="flask.cfg"):
    app = Flask(__name__, instance_relative_config=True)
    dnsmgr_config = os.getenv("DNSMGR_CONFIG", "production")
    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    try:
        os.makedirs(os.path.join(app.instance_path, "db"))
    except OSError:
        pass

    # use sqlite as default
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
        app.instance_path, "db", "app.db"
    )

    config = appconfig.DevelopmentConfig
    if dnsmgr_config == "production":
        config = appconfig.ProductionConfig
    if dnsmgr_config == "testing":
        config = appconfig.TestingConfig
    app.config.from_object(config)

    app.jinja_env.globals.update(app_version=version)
    initialize_extensions(app)
    register_blueprints(app)

    @app.route("/")
    def index():
        return render_template("index.html")  # pragma: no cover

    return app


def initialize_extensions(app):
    login = LoginManager()
    login.login_view = "user.login"

    import dnsmgr.models  # noqa: F401

    db.init_app(app)
    Migrate(app, db)

    bcrypt.init_app(app)
    login.init_app(app)
    app.app_context().push()

    db.create_all()

    from dnsmgr.models import User

    @login.user_loader
    def load_user(user_id):
        return User.query.filter(User.id == int(user_id)).first()


def register_blueprints(app):
    from dnsmgr.config import config_blueprint
    from dnsmgr.network import network_blueprint
    from dnsmgr.host import host_blueprint
    from dnsmgr.alias import alias_blueprint
    from dnsmgr.user import users_blueprint
    from dnsmgr.webhook import webhook_blueprint
    from dnsmgr.search import search_blueprint
    from dnsmgr.token import token_blueprint

    app.register_blueprint(config_blueprint)
    app.register_blueprint(network_blueprint)
    app.register_blueprint(host_blueprint)
    app.register_blueprint(alias_blueprint)
    app.register_blueprint(users_blueprint)
    app.register_blueprint(webhook_blueprint)
    app.register_blueprint(search_blueprint)
    app.register_blueprint(token_blueprint)
