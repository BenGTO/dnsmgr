import string
import re


def contains_whitespace(s: str):
    """
    Returns true if supplied string contains whitespaces
    """
    return True in [c in s for c in string.whitespace]


def is_valid_dns_name(s: str):
    """
    Returns true if string contains only alphanumeric characters, dots and dashes.

    Thank you for this regex, Brave Leo AI.
    """
    # fmt: off
    if re.match("^[a-zA-Z]([a-zA-Z0-9-]{1,63})(\\.[a-zA-Z0-9-]{1,63})*$", s):  # noqa: W605
        return True
    return False
    # fmt: on
