"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import jwt
from datetime import datetime, timedelta, timezone

min_token_length = 32
default_token_lifetime = 86400
max_token_lifetime = 3153600000  # 100 years
token_algorithm = "HS512"  # nosec B105


def token_encode(secret: str, expiration_sec=0, user_id=None, data=""):
    """
    Encode a token with payloads for expiration time and owner.
    :param: secret: global key for the encryption
    :param: expiration_sec: time in seconds until the token expires
    :param: user_id: user owning the token
    :param: optional extra data
    :returns: encode JWT token with fields `user_id`, `exp` (expiry date), `data`
    """
    if len(str(secret)) < min_token_length:
        raise ValueError(
            "Secret must be at least {0} characters long".format(min_token_length)
        )
    if expiration_sec < 0:
        raise ValueError("Token lifetime must be non-negative")
    if expiration_sec == 0:
        expiration_sec = max_token_lifetime
    expiration_datetime = datetime.now(tz=timezone.utc) + timedelta(
        seconds=expiration_sec
    )
    return jwt.encode(
        {"user_id": user_id, "exp": expiration_datetime, "data": data},
        secret,
        algorithm=token_algorithm,
    )


def token_decode(secret: str, token: str):
    """
    Decode a JWT token
    :returns: a dict with user_id, exp (expiry date), data
    """
    return jwt.decode(token, secret, algorithms=[token_algorithm])


class AuthToken:
    """
    JWT token wrapper.
    """

    def __init__(self, secret, expiration_sec=0, user_id=None, data="", token=None):
        """
        Create an AuthToken. This can be done by supplying an encrypted token, which will be decoded
        and copied to the object attributes. Or by data required to create a new token.

        :param: secret: global encryption key, always mandatory

        Parameters for initialisation by data
        :param: expiration_sec: time to live in seconds
        :param: user_id: owner ID of the token created
        :param: data: optional additional payload

        Parameters for initialisation by token
        :param: token: encrypted token

        """
        if user_id is not None:
            self.token = token_encode(secret, expiration_sec, user_id, data)
            self.user_id = user_id
            self.data = data
            self.expiration_sec = expiration_sec
        elif token is not None:
            self.token = token
            t = token_decode(secret, token)
            self.user_id = t["user_id"]
            self.data = t["data"]
            self.expiration_sec = t["exp"]
        else:
            raise ValueError("Neither user_id nor token defined")

    def __eq__(self, other):
        return self.token == other.token

    def expiry_date(self, creation_date):
        """
        Calculate the expiration date of the token
        :param: creation_date: datetime object holding the time of creation
        :returns: datetime object with the date of expiry
        """
        return creation_date + timedelta(seconds=self.expiration_sec)
