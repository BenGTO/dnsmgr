"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import request, redirect, flash, url_for
from flask_login import current_user
from dnsmgr.models import User
from functools import wraps

user_roles = ["user", "admin", "readonly"]

user_role_unknown = "Unknown user role"
user_name_invalid = "Invalid user name, non alphanumeric character detected"
user_name_exists = "Name already exists"


def admin_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        user = User.query.filter_by(id=current_user.id).first()
        if user.role != "admin":
            flash("You must be admin", "error")
            if request.referrer is not None:
                return redirect(request.referrer)
            else:
                return redirect(url_for("network.index"))

        return func(*args, **kwargs)

    return decorated_view


def readonly_denied(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        user = User.query.filter_by(id=current_user.id).first()
        if user.role == "readonly":
            flash("You only have read access", "error")
            if request.referrer is not None:
                return redirect(request.referrer)
            else:
                return redirect(url_for("network.index"))

        return func(*args, **kwargs)

    return decorated_view
