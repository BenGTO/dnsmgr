"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, RadioField
from wtforms.fields.simple import HiddenField
from wtforms.validators import DataRequired, Length, EqualTo, Regexp
from dnsmgr.models import User
from .roles import user_roles

DESIGN_CHOICES = [("default", "Light mode"), ("dark", "Dark mode")]


class PasswordForm(FlaskForm):
    password = PasswordField(
        "Password",
        validators=[
            DataRequired(),
            Length(min=6, max=40),
            Regexp(r"^\S+$", message="Passwords may not contain whitespace"),
        ],
    )
    confirm = PasswordField(
        "Confirm Password", validators=[DataRequired(), EqualTo("password")]
    )
    submit = SubmitField("Update")


class ProfileForm(FlaskForm):
    design = RadioField("GUI design", choices=DESIGN_CHOICES, default="default")
    submit = SubmitField("Update")


class LoginForm(FlaskForm):
    login = StringField("Login", validators=[DataRequired(), Length(min=4, max=100)])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Login")


class EditForm(FlaskForm):
    # login = StringField("Login", validators=[DataRequired(), Length(min=4, max=100)], render_kw={'readonly': True})
    id = HiddenField()
    login = StringField(
        "Login",
        validators=[
            DataRequired(),
            Regexp(r"^\w*$", message="Login names may not contain whitespace"),
        ],
    )
    role = RadioField(
        "Role", validators=[DataRequired()], choices=user_roles, default="user"
    )
    password = PasswordField(
        "Password",
        validators=[Regexp(r"^\w*$", message="Passwords may not contain whitespace")],
    )
    confirm = PasswordField("Confirm Password", validators=[EqualTo("password")])
    submit = SubmitField("Save user")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Edit User"


class AddForm(EditForm):
    submit = SubmitField("Add user")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title = "Add User"
        self.user = None

    def validate(self, extra_validators=None):
        rv = FlaskForm.validate(self, extra_validators)
        if not rv:
            return False

        user = User.query.filter_by(login=self.login.data).first()
        if user is not None:
            if user.login == self.login.data:
                self.login.errors.append("Duplicate name")
                return False
        return True


class DeleteForm(FlaskForm):
    id = HiddenField()
    login = HiddenField()
    submit = SubmitField("Delete user")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Delete User"
