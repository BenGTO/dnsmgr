"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
"""
The user Blueprint handles the user management for this application.

Specifically, this Blueprint allows for new user to register and for
user to log in and to log out of the application.
"""
from flask import Blueprint, current_app, request, abort
from flask_login import current_user, login_user

from dnsmgr.models import Token, User
from dnsmgr.cryptography.auth_token import AuthToken

users_blueprint = Blueprint("user", __name__, template_folder="templates")
from . import routes  # noqa: F401


@current_app.before_request
def before_request():
    """
    Intercept unauthenticated requests and search for an access token. If found, try to decode
    with the application SECRET_KEY. Take the user ID in the payload to grant access to this
    user.
    """
    # Ignore if authenticated
    if not current_user.is_authenticated:
        auth_header = request.headers.get("Authorization")
        if auth_header is None or "Bearer" not in auth_header:
            return

        # we have a token. Authenticate or die (403)
        try:
            received_token = auth_header.split(" ")[1]
        except IndexError:
            return
        token = Token.query.filter(Token.token == received_token).first()

        if token:
            # Unpack token
            t = AuthToken(secret=current_app.config["SECRET_KEY"], token=received_token)
            user = User.query.filter(Token.id == t.user_id).first()
            if user:
                # User ID from token matched, give access to this user
                login_user(user)
                return

        abort(403)
