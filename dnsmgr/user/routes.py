"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import render_template, request, flash, redirect, url_for
from flask_login import (
    login_user,
    current_user,
    login_required,
    logout_user,
    fresh_login_required,
)
from urllib.parse import urlparse
import re

from . import users_blueprint
from .forms import ProfileForm, LoginForm, PasswordForm, EditForm, AddForm, DeleteForm
from .roles import admin_required
from dnsmgr.models import User
from dnsmgr import db


@users_blueprint.route("/profile", methods=["GET", "POST"])
@login_required
def profile():
    context = {
        "breadcrumbs": [{"target": url_for("network.index"), "label": "Networks"}]
    }
    context["form"] = ProfileForm(obj=context)
    user = current_user
    if request.method == "GET":
        context["form"].design.data = user.design

    if request.method == "POST" and context["form"].validate_on_submit():
        context["form"].populate_obj(user)
        db.session.commit()
        flash("Settings updated", "success")
    return render_template("user/profile.html", context=context)


@users_blueprint.route("/password", methods=["GET", "POST"])
@fresh_login_required
def password():
    context = {
        "breadcrumbs": [{"target": url_for("network.index"), "label": "Networks"}]
    }
    context["form"] = PasswordForm(obj=context)
    if request.method == "POST" and context["form"].validate_on_submit():
        user = User.query.filter_by(id=current_user.id).first()
        if re.match(r"^\S+$", context["form"].password.data):
            user.set_password(context["form"].password.data)
            db.session.commit()
            flash("Password changed", "success")
        else:
            flash("Passwords may not contain whitespace")
    return render_template("user/password.html", context=context)


@users_blueprint.route("/login", methods=["GET", "POST"])
def login():
    # Initial login after fresh installation: create default admin user
    try:
        new_user = User("admin", "password", role="admin")
        db.session.add(new_user)
        db.session.commit()
        flash(
            "Defaults login created. Remember to change the password."
        )  # pragma: no cover
    except Exception:
        db.session.rollback()

    context = {
        "breadcrumbs": [{"target": url_for("network.index"), "label": "Networks"}]
    }
    # If the User is already logged in, don't allow them to try to log in again
    if current_user.is_authenticated:
        flash("Already logged in! Redirecting to your User Profile page...")
        return redirect(url_for("user.profile"))

    context["form"] = LoginForm()

    if request.method == "POST":
        if context["form"].validate_on_submit():
            user = User.query.filter_by(login=context["form"].login.data).first()
            if user and user.is_correct_password(context["form"].password.data):
                user.authenticated = True
                db.session.add(user)
                db.session.commit()
                login_user(user, remember=context["form"].remember_me.data)
                flash(
                    "Thanks for logging in, {}!".format(current_user.login), "success"
                )

                # Upgrade role of user "admin" if legacy user entry has not yet role "admin"
                if user.login == "admin" and user.role != "admin":
                    user.role = "admin"
                    db.session.commit()

                return redirect(
                    url_for(
                        "network.index",
                        _external=True,
                        _scheme=urlparse(request.url).scheme,
                    )
                )

        flash("ERROR! Incorrect login credentials.", "error")
    return render_template("user/login.html", context=context)  # pragma: no cover


@users_blueprint.route("/logout")
@login_required
def logout():
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    flash("Goodbye!", "success")
    return redirect(url_for("user.login"))


@users_blueprint.route("/user")
@login_required
@admin_required
def users():
    context = {
        "breadcrumbs": [{"target": url_for("network.index"), "label": "Networks"}],
        "users": User.query.all(),
    }
    return render_template("user/index.html", context=context)


@users_blueprint.route("/user/add/<string:id>", methods=["GET", "POST"])
@login_required
@admin_required
def add(id):
    context = {
        "breadcrumbs": [{"target": url_for("user.users"), "label": "Users"}],
        "form": AddForm(),
    }

    if request.method == "POST" and context["form"].validate_on_submit():
        myself = User.query.filter_by(id=current_user.id).first()
        if myself.role != "admin":
            flash("You need to have admin role to manage user", "error")
            return render_template("networks")
        if context["form"].validate_on_submit():
            user = User()
            context["form"].populate_obj(user)
            user = User(user.login, user.password, user.role, user.design)
            try:
                db.session.add(user)
                db.session.commit()
            except Exception:  # pragma: no cover
                flash("Could not add user", "error")  # pragma: no cover
                return render_template(
                    "user/add.html", context=context
                )  # pragma: no cover
            flash("User added")
        return redirect(url_for("user.users"))
    return render_template("user/edit.html", context=context)


@users_blueprint.route("/user/edit/<string:id>", methods=["GET", "POST"])
@login_required
@admin_required
def edit(id):
    context = {"breadcrumbs": [{"target": url_for("user.users"), "label": "Users"}]}

    user = User.query.filter_by(id=id).first()
    context["form"] = EditForm(obj=user)
    context["user"] = user
    if request.method == "POST":
        if context["form"].validate_on_submit():
            # Keep old password if form field is empty
            old_hashed_password = user.hashed_password
            if user:
                # Password is also checked by form validator, may be empty, but without whitespace
                if (
                    context["form"].password.data
                    and len(context["form"].password.data) > 0
                    and re.match(r"^\w*$", context["form"].password.data)
                ):
                    user.set_password(context["form"].password.data)
                    flash("New password set")
                else:
                    context["user"].hashed_password = old_hashed_password
            context["form"].populate_obj(context["user"])

            try:  # this should never fail after form validation
                db.session.commit()
            except Exception:  # pragma: no cover
                flash("Could not update user", "error")  # pragma: no cover
                return render_template(
                    "user/edit.html", context=context
                )  # pragma: no cover
            return redirect(url_for("user.users"))

    if request.method == "GET":
        if context["user"] is None:
            flash("User not found")
            return render_template("user/index.html", context=context)

    return render_template("user/edit.html", context=context)


@users_blueprint.route("/user/delete/<string:id>", methods=["GET", "POST"])
@login_required
@admin_required
def delete(id):
    context = {}
    user = User.query.filter_by(id=id).first()
    if not user:
        flash("Attempted to delete a non existent user", "error")
        return render_template("user/index.html", context=context)

    context["user"] = user
    context["breadcrumbs"] = [{"target": url_for("user.users"), "label": "Users"}]
    context["form"] = DeleteForm(obj=context["user"])

    if request.method == "POST":
        db.session.delete(context["user"])
        db.session.commit()

        return redirect(url_for("user.users"))

    return render_template("user/delete.html", context=context)  # pragma: no cover
