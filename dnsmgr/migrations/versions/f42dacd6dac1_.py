"""empty message

Revision ID: f42dacd6dac1
Revises: cbf90cf908fe
Create Date: 2020-11-14 23:59:25.651988

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.exc import OperationalError


# revision identifiers, used by Alembic.
revision = "f42dacd6dac1"
down_revision = "cbf90cf908fe"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    try:
        op.add_column(
            "network",
            sa.Column("dhcp_nextserver", sa.String(length=255), nullable=True),
        )
    except OperationalError:
        pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    try:
        op.drop_column("network", "dhcp_nextserver")
    except OperationalError:
        pass
    # ### end Alembic commands ###
