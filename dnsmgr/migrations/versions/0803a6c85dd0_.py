"""empty message

Revision ID: 0803a6c85dd0
Revises: cfabe82dac1b
Create Date: 2021-05-11 15:02:38.573459

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.exc import OperationalError

# revision identifiers, used by Alembic.
revision = "0803a6c85dd0"
down_revision = "cfabe82dac1b"
branch_labels = None
depends_on = None


def upgrade():
    try:
        op.alter_column("host", "mac", nullable=True, existing_type=sa.String(25))
    except OperationalError:
        pass


def downgrade():
    try:
        op.alter_column("host", "mac", nullable=False, existing_type=sa.String(25))
    except OperationalError:
        pass
