"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask_login import current_user
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, TextAreaField
from wtforms.validators import DataRequired, Length
from duration_parser import parse
from dnsmgr.models import Token


class AddForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired(), Length(min=1, max=64)])
    description = StringField("Description (optional)")
    user_id = HiddenField()
    expiration = StringField(
        "Lifetime",
        default="1y",
        validators=[DataRequired()],
        description="i.e. 30s, 1m, 1h, 1y",
    )
    submit = SubmitField("Generate")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Add token"

    def validate(self, extra_validators=None):
        FlaskForm.validate(self, extra_validators)
        try:
            parse(self.expiration.data)
        except ValueError:
            self.expiration.errors.append("Invalid time specification")
            return False

        token = Token.query.filter(
            Token.user_id == current_user.id, Token.name == self.name.data
        ).count()
        if token:
            self.name.errors.append("You already have a token of that name")
            return False
        return True


class DeleteForm(FlaskForm):
    token_id = HiddenField()
    submit = SubmitField("Delete")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Delete token"


class ShowForm(FlaskForm):
    id = HiddenField()
    token = TextAreaField(render_kw={"readonly": True, "rows": 4, "cols": 80})
