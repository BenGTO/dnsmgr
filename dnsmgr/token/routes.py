"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import (
    render_template,
    request,
    flash,
    redirect,
    url_for,
    current_app as app,
)
from flask_login import login_required, current_user
from duration_parser import parse
from dnsmgr import db
from dnsmgr.models import Token, User
from dnsmgr.cryptography.auth_token import AuthToken
from . import token_blueprint
from .forms import AddForm, DeleteForm, ShowForm


@token_blueprint.route("/token_list", methods=["GET"])
@login_required
def token_list():
    context = {}

    user = User.query.filter_by(id=current_user.id).first()
    context["token_list"] = Token.query.filter_by(user_id=user.id)
    context["user"] = current_user
    context["breadcrumbs"] = [{"target": url_for("network.index"), "label": "Networks"}]

    return render_template("token/index.html", context=context)


@token_blueprint.route("/token/add", methods=["GET", "POST"])
@login_required
def add():
    context = {
        "breadcrumbs": [{"target": url_for("token.token_list"), "label": "Token list"}]
    }
    # Add form uses AuthToken
    context["token"] = AuthToken(
        secret=app.config["SECRET_KEY"], user_id=current_user.id
    )
    context["form"] = AddForm(obj=context["token"])
    if request.method == "POST" and context["form"].validate_on_submit():
        # Token user ID must match current user ID
        if context["token"].user_id != current_user.id:
            flash("Token has an invalid user ID", "error")  # pragma: no cover
            return render_template("token/add.html", context=context)
        context["form"].populate_obj(context["token"])
        # Form validation rejects this, but functional tests bypass form
        if context["form"].expiration.data == "":
            flash("Invalid time specification", "error")  # pragma: no cover
            return render_template(
                "token/add.html", context=context
            )  # pragma: no cover

        # Show form uses Token
        context["token"] = Token(
            name=context["form"].name.data,
            expiration_sec=parse(context["form"].expiration.data),
            user_id=context["token"].user_id,
            data=context["token"].data,
            description=context["form"].description.data,
        )
        db.session.add(context["token"])
        try:
            db.session.commit()
        # should not fail after proper form validation. But hardware may fail...
        except Exception:  # pragma: no cover
            flash("Could not add token", "error")  # pragma: no cover
            return render_template(
                "token/add.html", context=context
            )  # pragma: no cover

        return redirect(url_for("token.show", token_id=context["token"].id))
        # return render_template("token/show.html", context=context)

    return render_template("token/add.html", context=context)


@token_blueprint.route("/token/show/<int:token_id>", methods=["GET"])
@login_required
def show(token_id):
    context = {}
    context = {
        "breadcrumbs": [{"target": url_for("token.token_list"), "label": "Token list"}]
    }
    context["token"] = Token.query.filter_by(id=token_id).first()
    context["form"] = ShowForm(obj=context["token"])
    # return redirect(url_for("token.show"))
    return render_template("token/show.html", context=context)


@token_blueprint.route("/token/delete/<string:token_id>", methods=["GET", "POST"])
@login_required
def delete(token_id):
    context = {
        "breadcrumbs": [{"target": url_for("token.token_list"), "label": "Token list"}]
    }
    context["token"] = Token.query.filter_by(id=token_id).first()

    context["form"] = DeleteForm(obj=context["token"])
    if request.method == "POST":
        db.session.delete(context["token"])
        db.session.commit()

        return redirect(url_for("token.token_list", context=context))

    return render_template("token/delete.html", context=context)  # pragma: no cover
