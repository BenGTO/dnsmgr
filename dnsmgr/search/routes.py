"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import render_template, request, url_for
from flask_login import login_required
from . import search_blueprint
from .forms import SearchForm
from sqlalchemy import or_
from dnsmgr.models import Host, Alias


@search_blueprint.route("/search", methods=["GET", "POST"])
@login_required
def index():
    context = {
        "breadcrumbs": [{"target": url_for("network.index"), "label": "Networks"}]
    }

    context["form"] = SearchForm()
    # user had entered a query
    if request.method == "POST" and context["form"].validate_on_submit():
        pattern = context["form"].searchpattern.data
        context["searchresult"] = Host.query.filter(
            or_(
                Host.name.ilike("%{0}%".format(pattern)),
                Host.mac.ilike("%{0}%".format(pattern)),
                Host.comment.ilike("%{0}%".format(pattern)),
                Host.aliases.any(Alias.name.ilike("%{0}%".format(pattern))),
            )
        )

    return render_template("search/search.html", context=context)  # pragma: no cover
