"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import render_template, url_for
from flask_login import login_required
from . import webhook_blueprint

# from .forms import AddForm, EditForm, DeleteForm
from dnsmgr.models import Config
import requests
from requests.exceptions import MissingSchema, HTTPError, ConnectionError


@webhook_blueprint.route("/webhook", methods=["GET"])
@login_required
def index():
    context = {}
    breadcrumbs = [{"target": url_for("network.index"), "label": "Networks"}]
    context["breadcrumbs"] = breadcrumbs

    context["config"] = Config.query.all()[0]
    try:
        # Launches an external program, may take some time to complete: no timeout here
        r = requests.get(context["config"].webhook)  # nosec B113
        requests.Response.raise_for_status(r)
        context["response"] = r.text
    except ConnectionError:
        context["response"] = "Connection error (no server found)"
    except MissingSchema:
        context["response"] = "ERROR: URL incomplete, schema is missing"
    except HTTPError:
        context["response"] = "ERROR: got return code {0}: {1}".format(
            r.status_code, r.reason
        )
    except Exception as exp:  # pragma: no cover
        template = (
            "An exception of type {0} occurred. Arguments:\n{1!r}"  # pragma: no cover
        )
        context["response"] = template.format(
            type(exp).__name__, exp.args
        )  # pragma: no cover
    else:
        pass

    return render_template("webhook/index.html", context=context)  # pragma: no cover
