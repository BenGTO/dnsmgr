"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys
import os.path

# Show deprecation warnings, because deployment configuration should be fixed
if not sys.warnoptions:
    import os
    import warnings

    warnings.simplefilter("default")  # Change the filter in this process
    os.environ["PYTHONWARNINGS"] = "default"  # Also affect subprocesses


class Config(object):
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = os.getenv("SECRET_KEY", os.urandom(32).hex())
    RESTRICT_ACCESS_TO_DUMPS = os.getenv("RESTRICT_ACCESS_TO_DUMPS", False)
    DEBUG = os.getenv("DEBUG", None)
    TESTING = os.getenv("TESTING", None)
    ENV = os.getenv("FLASK_ENV", None)
    WTF_CSRF_ENABLED = False

    db_driver = "sqlite"
    db_type = os.getenv("DB_TYPE", "sqlite")
    db_host = os.getenv("DB_HOST", None)
    db_name = os.getenv("DB_NAME", None)
    db_port = os.getenv("DB_PORT", "3306")
    db_user = os.getenv("DB_USER", "dnsmgr")
    db_path = os.getenv("DB_PATH")
    db_password = os.getenv("DB_PASSWORD", None)
    if db_host is not None and len(db_host) > 0:
        if db_type == "mysql":
            db_driver = "mysql+pymysql"
        if db_type == "postgres" or db_type == "postgresql":
            db_port = os.getenv("DB_PORT", "5432")
            db_driver = "postgresql+psycopg2"
            if db_type == "postgres":
                warnings.warn(
                    "DB_TYPE 'postgres' is deprecated, use 'postgresql'",
                    DeprecationWarning,
                )

        SQLALCHEMY_DATABASE_URI = "%s://%s:%s@%s:%s/%s" % (
            db_driver,
            db_user,
            db_password,
            db_host,
            db_port,
            db_name,
        )
    elif db_path is not None and len(db_path) > 0:
        SQLALCHEMY_DATABASE_URI = "sqlite:///{0}".format(db_path)
    else:
        SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"


class ProductionConfig(Config):
    ENV = "production"
    WTF_CSRF_ENABLED = True


class TestingConfig(Config):
    ENV = "testing"


class DevelopmentConfig(Config):
    DEBUG = True
    ENV = "development"
