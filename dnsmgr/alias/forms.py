"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField
from wtforms.validators import DataRequired, Length
from dnsmgr.models import Host, Alias
from dnsmgr.helpers.string_checks import is_valid_dns_name


class EditForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired(), Length(min=1, max=255)])
    host_id = HiddenField()
    submit = SubmitField("Save alias")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Edit alias"
        self.alias = None

    def validate(self, extra_validators=None):
        rv = FlaskForm.validate(self)
        if not rv:  # pragma: no cover
            return False  # pragma: no cover

        if not is_valid_dns_name(self.name.data):
            self.name.errors.append("Invalid character(s) detected")
            return False

        host = Host.query.filter_by(id=self.host_id.data).first()
        alias = Alias.query.filter_by(name=self.name.data).first()
        if alias in host.aliases:
            return True

        host = Host.query.filter_by(name=self.name.data).first()
        alias = Alias.query.filter_by(name=self.name.data).first()

        if host is not None:
            self.name.errors.append("Name already exists as host")
            return False
        if alias is not None:
            self.name.errors.append("Name already exists as alias")
            return False

        return True


class AddForm(EditForm):
    submit = SubmitField("Add alias")

    def __init__(self, *args, **kwargs):
        EditForm.__init__(self, *args, **kwargs)
        self.title = "Add alias"

    def validate(self, extra_validators=None):
        rv = EditForm.validate(self)
        if not rv:
            return False

        alias = Alias.query.filter_by(name=self.name.data).first()
        if alias is not None:
            self.name.errors.append("Alias already exists")
            return False

        return True


class DeleteForm(FlaskForm):
    host = HiddenField()
    name = HiddenField()
    submit = SubmitField("Delete alias")

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        self.title = "Delete alias"
