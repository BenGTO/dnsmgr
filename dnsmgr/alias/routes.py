"""
Copyright (C) 2025 Bengt Giger

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from flask import render_template, request, redirect, url_for, flash
from flask_login import login_required
from . import alias_blueprint
from .forms import EditForm, AddForm, DeleteForm
from dnsmgr.models import Alias, Host
from dnsmgr.user.roles import readonly_denied
from dnsmgr import db


@alias_blueprint.route("/alias/add/<string:host_address>", methods=["GET", "POST"])
@login_required
@readonly_denied
def add(host_address):
    context = {}
    context["host"] = Host.query.filter_by(address=host_address).first()
    context["alias"] = Alias()
    context["alias"].host_id = context["host"].id
    context["breadcrumbs"] = [
        {"target": url_for("network.index"), "label": "Networks"},
        {
            "target": url_for(
                "host.host_list", network_address=context["host"].network.address
            ),
            "label": "Host list",
        },
        {
            "target": url_for("alias.alias_list", host_address=context["host"].address),
            "label": "Host",
        },
    ]

    context["form"] = AddForm(obj=context["alias"])

    if request.method == "POST" and context["form"].validate_on_submit():
        context["form"].populate_obj(context["alias"])
        db.session.add(context["alias"])
        try:
            db.session.commit()

        # should not fail after proper form validation. But networks may fail...
        except Exception:  # pragma: no cover
            flash("Could not add alias (duplicate?)", "error")  # pragma: no cover
            return render_template(
                "alias/edit.html", context=context
            )  # pragma: no cover

        return redirect(
            url_for("alias.alias_list", host_address=context["host"].address)
        )
    return render_template("alias/edit.html", context=context)


@alias_blueprint.route("/alias/edit/<string:alias_name>", methods=["GET", "POST"])
@login_required
@readonly_denied
def edit(alias_name):
    context = {}
    context["alias"] = Alias.query.filter_by(name=alias_name).first()
    context["host"] = Host.query.filter_by(id=context["alias"].host_id).first()
    context["breadcrumbs"] = [
        {"target": url_for("network.index"), "label": "Networks"},
        {
            "target": url_for(
                "host.host_list", network_address=context["host"].network.address
            ),
            "label": "Host list",
        },
        {
            "target": url_for("alias.alias_list", host_address=context["host"].address),
            "label": "Host",
        },
    ]

    context["form"] = EditForm(obj=context["alias"])
    if request.method == "POST" and context["form"].validate_on_submit():
        context["form"].populate_obj(context["alias"])
        try:
            db.session.commit()
        except Exception:  # pragma: no cover
            flash("Could not update alias", "error")  # pragma: no cover
            return render_template(
                "alias/edit.html", context=context
            )  # pragma: no cover

        return redirect(
            url_for(
                "alias.alias_list",
                host_address=context["host"].address,
                network_address=context["host"].network.address,
            )
        )

    return render_template("alias/edit.html", context=context)


@alias_blueprint.route("/alias/list/<string:host_address>", methods=["GET", "POST"])
@login_required
def alias_list(host_address):
    context = {}
    context["host"] = Host.query.filter_by(address=host_address).first()
    context["aliases"] = Alias.query.filter_by(host=context["host"]).order_by(
        Alias.name
    )
    context["breadcrumbs"] = [
        {"target": url_for("network.index"), "label": "Networks"},
        {
            "target": url_for(
                "host.host_list", network_address=context["host"].network.address
            ),
            "label": "Host list",
        },
    ]

    return render_template("alias/aliaslist.html", context=context)


@alias_blueprint.route("/alias/delete/<string:alias_name>", methods=["GET", "POST"])
@login_required
@readonly_denied
def delete(alias_name):
    context = {}
    context["alias"] = Alias.query.filter_by(name=alias_name).first()
    context["host"] = context["alias"].host
    context["breadcrumbs"] = [
        {"target": url_for("network.index"), "label": "Networks"},
        {
            "target": url_for(
                "host.host_list", network_address=context["host"].network.address
            ),
            "label": "Host list",
        },
        {
            "target": url_for("alias.alias_list", host_address=context["host"].address),
            "label": "Host",
        },
    ]

    context["form"] = DeleteForm(obj=context["alias"])
    if request.method == "POST":
        db.session.delete(context["alias"])
        db.session.commit()

        return redirect(
            url_for("alias.alias_list", host_address=context["host"].address)
        )

    return render_template("alias/delete.html", context=context)  # pragma: no cover
