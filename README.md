
[![pipeline status](https://gitlab.com/BenGTO/dnsmgr/badges/master/pipeline.svg)](https://gitlab.com/BenGTO/dnsmgr/-/commits/master) [![coverage report](https://gitlab.com/BenGTO/dnsmgr/badges/master/coverage.svg)](https://gitlab.com/BenGTO/dnsmgr/-/commits/master)

DNS Manager for Home Networks
=============================

Just enough for the network at home.

## Features
- Manages multiple networks
- Lease time, name server, boot server per network
- IP ranges for dynamic IPs
- Search for host names, aliases and MAC addresses
- Basic user management
- provides consistent configuration files for
  - ISC dhcp server
  - bind DNS server

Read the [user documentation here](https://dnsmgr.readthedocs.io/en/latest/).

Installation
============
## Kubernetes
See [deploy/kustomize](deploy/kustomize/README.md).

## Docker Compose
See [deploy/compose](deploy/compose/README.md).

Configuration
=============

## Environment Variables
 - `DB_HOST`: set this to the hostname of the database host. If left emtpy,
   a local sqlite database will be used.
 - `DB_TYPE`: type of database server, may be set to `sqlite` (default) or `mysql`
 - `DB_NAME`: database name
 - `DB_PORT`: port to connect for database access
 - `DB_USER`, `DB_PASSWORD`: credentials for the database
 - `DB_PATH`: path to the database file if sqlite is used. Defaults to
   `<install_dir>/instance/db`. In Docker containers, you can mount a
   volume to `/app/instance/db` to store the database on a persistent volume.

Usage
=====

Login with username _admin_ and password _password_. Change the password in the
user profile. Then open the settings (cog wheel icon)
and enter the basic information for your environment.

Add networks, then hosts and aliases.

Output
======
## dhcpd Configuration

The URL [http://server/network/dhcpd.conf](/network/dhcpd.conf) can be retrieved
and saved as configuration for the _ISC dhcpd_.

## bind Configuration

Similarly, the URL [http://server/network/db.site](/network/db.site) provides a
configration file for bind. Additionally, a reverse bind configuration is available
for every subnet, with URLs like [http://server/network/db.rev/192.168.0.0](/network/db.rev/192.168.0.0).
They can be included in a local bind configuration:

```
       zone "my.net" {
                type master;
                file "/etc/bind/db.site";
        };
        zone "0.168.192.in-addr.arpa" {
                type master;
                file "/etc/bind/192.168.0.0";
        };
        ...
```

Development
=============
## DB Schema

*dnsmgr* uses Alembic to keep track of the DB schema. Whenever changes to the DB
schema have to be made during development, a new revision has to be created.
A revision contains a patch as Python Code.

**Warning:** the change detection works with addition and removal of tables
and columns, but not with name changes. If you change a column or table name,
check and fix the patch code as described below.

### Schema Upgrade Patch

 1. load environment with venv\Scripts\activate.bat or venv/bin/activate
 1. set variable: `set FLASK_APP=dnsmgr.py` for Windows or `export FLASK_APP=dnsmgr.py`
 for Linux.
 1. create DB schema patch: `flask db migrate -m "<commit message"`, check generated Python
 file in directory `versions`.

### Apply Patch

Installations using a different (usually older)  DB schema have to be
upgraded by applying the generated (maybe edited) patch file.
To apply the patch, you have to run `flask db upgrade`.

Patch application is **idempotent**, if the DB schema is up to date, no
action will be taken.

It is the responsibility of the installer or start script to ensure that
the version is updated. See the project Dockerfile `CMD` as an example.
