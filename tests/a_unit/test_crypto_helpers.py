import datetime
import unittest
import jwt
import time
from dnsmgr.cryptography.auth_token import (
    token_decode,
    token_encode,
    AuthToken,
    min_token_length,
)

default_secret = "x" * 128
minimal_secret = "x" * min_token_length
short_secret = "x" * (min_token_length - 1)


class TestAuthToken(unittest.TestCase):
    def test_empty_secret(self):
        self.assertRaises(
            ValueError,
            token_encode,
            secret="",
            data="data",
            expiration_sec=1,
            user_id=1,
        )

    def test_short_secret(self):
        self.assertRaises(
            ValueError,
            token_encode,
            secret=short_secret,
            expiration_sec=1,
            data="data",
            user_id=1,
        )

    def test_negative_lifetime(self):
        self.assertRaises(
            ValueError,
            token_encode,
            secret=minimal_secret,
            expiration_sec=-1,
            data="data",
            user_id=1,
        )

    def test_valid_arguments(self):
        self.assertTrue(
            token_encode(default_secret, expiration_sec=1, data="data", user_id=1)
        )

    def test_good_decode(self):
        token = token_encode(default_secret, expiration_sec=1, data="data", user_id=1)
        data = token_decode(default_secret, token)
        self.assertEqual(data["data"], "data")

    def test_invalid_token(self):
        token = token_encode(default_secret, expiration_sec=1, data="data", user_id=1)
        self.assertRaises(
            jwt.exceptions.InvalidSignatureError,
            token_decode,
            secret=minimal_secret,
            token=token,
        )

    def test_short_garbage_token(self):
        self.assertRaises(
            jwt.exceptions.DecodeError, token_decode, default_secret, token="abcde"
        )

    def test_long_garbage_token(self):
        self.assertRaises(
            jwt.exceptions.DecodeError,
            token_decode,
            default_secret,
            token="abcde" * 100,
        )

    def test_expired_token(self):
        token = token_encode(default_secret, expiration_sec=1, data="data", user_id=1)
        time.sleep(2)
        self.assertRaises(
            jwt.exceptions.ExpiredSignatureError,
            token_decode,
            default_secret,
            token=token,
        )

    def test_user_id(self):
        for user_id in (-1, 0, 1, 1000000):
            token = token_encode(
                default_secret, expiration_sec=1, data="data", user_id=user_id
            )
            data = token_decode(default_secret, token)
            self.assertEqual(data["user_id"], user_id)

    def test_z_create_from_data(self):
        token = AuthToken(default_secret, 1, user_id=1, data="data")
        self.assertIsNotNone(token)

    def test_z_create_from_token(self):
        t = token_encode(default_secret, 100, user_id=1, data="data")
        token = AuthToken(default_secret, token=t)

    def test_z_compare_token(self):
        t1 = token_encode(default_secret, 100, user_id=1, data="data")
        t2 = token_encode(default_secret, 100, user_id=1, data="data")
        self.assertTrue(t1 == t2)

    def test_z_expiry_date_diff(self):
        creation_date = datetime.datetime.now()
        t1 = AuthToken(default_secret, 100, user_id=1, data="data")
        t2 = AuthToken(default_secret, 200, user_id=1, data="data")

        diff = datetime.timedelta(seconds=100)

        self.assertEqual(
            t2.expiry_date(creation_date), t1.expiry_date(creation_date) + diff
        )

    def test_z_expiry_date_abs(self):
        creation_date = datetime.datetime.now()
        t1 = AuthToken(default_secret, 31536000, user_id=1, data="data")
        guli = t1.expiry_date(datetime.datetime.now())
        self.assertLess(
            t1.expiry_date(datetime.datetime.now()),
            datetime.datetime.now() + datetime.timedelta(seconds=31536010),
        )
        self.assertGreater(
            t1.expiry_date(datetime.datetime.now()),
            datetime.datetime.now() + datetime.timedelta(seconds=31535990),
        )
