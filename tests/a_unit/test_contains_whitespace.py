import unittest
from dnsmgr.helpers.string_checks import contains_whitespace


class TestContainsWhitespace(unittest.TestCase):
    def test_no_whitespace(self):
        self.assertFalse(contains_whitespace("abcde:fghij/klm_nop-qrs"))

    def test_spaces(self):
        self.assertTrue(contains_whitespace("a b"))

    def test_tabs(self):
        self.assertTrue(contains_whitespace("a  b"))

    def test_newline(self):
        self.assertTrue(
            contains_whitespace(
                """
        """
            )
        )


if __name__ == "__main__":  # pragma: no cover
    unittest.main()  # pragma: no cover
