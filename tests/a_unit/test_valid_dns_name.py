import unittest
from dnsmgr.helpers.string_checks import is_valid_dns_name


class TestContainsWhitespace(unittest.TestCase):
    def test_valid_dns_name(self):
        self.assertTrue(is_valid_dns_name("abcde"))
        self.assertTrue(is_valid_dns_name("abc2de"))
        self.assertTrue(is_valid_dns_name("abc2-de"))
        self.assertTrue(is_valid_dns_name("ab.bc"))
        self.assertTrue(is_valid_dns_name("ab.b.c"))

    def test_invalid_dns_name(self):
        self.assertFalse(is_valid_dns_name("a"))
        self.assertFalse(is_valid_dns_name("2abc"))
        self.assertFalse(is_valid_dns_name("a_bc"))
        self.assertFalse(is_valid_dns_name("a+bc"))
        self.assertFalse(is_valid_dns_name("a/bc"))
        self.assertFalse(is_valid_dns_name("ab..bc"))
        self.assertFalse(is_valid_dns_name("-abc"))
