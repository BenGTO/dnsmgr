import pytest
from flask_login import LoginManager


@pytest.mark.parametrize(
    ("login", "password", "message"),
    (
        ("", "", b"Incorrect login credentials"),
        ("a", "", b"Incorrect login credentials"),
        ("", "b", b"Incorrect login credentials"),
        ("a", "b", b"Incorrect login credentials"),
        ("admin", "password", b"Thanks for logging in"),
    ),
)
def test_login_validate_input(app_minimal, auth, login, password, message):
    auth.logout()
    response = auth.login(login, password)
    assert message in response.data
    response = auth.logout()
    assert b"<h1>Login</h1>" in response.data


def test_password_change(auth, client):
    auth.logout()

    auth.login("admin", "password")

    # Passwords differ
    response = client.post(
        "/password",
        data=dict(password="password_new", confirm="password_bad"),
        follow_redirects=True,
    )
    assert b"Field must be equal to password" in response.data

    # Passwords match
    response = client.post(
        "/password",
        data=dict(password="password_new", confirm="password_new"),
        follow_redirects=True,
    )
    assert b"Password changed" in response.data

    # Set password back to default
    response = client.post(
        "/password",
        data=dict(password="password", confirm="password"),
        follow_redirects=True,
    )
