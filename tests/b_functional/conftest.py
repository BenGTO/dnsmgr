import pytest
from flask import Flask
from bs4 import BeautifulSoup
from io import StringIO
import pandas as pd

from dnsmgr import create_app
from tests.b_functional.testitems import (
    default_network,
    default_host,
    temp_network,
    network_id,
)

credentials = dict(login="admin", password="password")


class Networks(object):
    def __init__(self, client):
        self._client = client

    def network_id(self):
        rv = self._client.get("/network/192.168.0.0")
        soup = BeautifulSoup(rv.data, "html.parser")
        return soup.find(id="network_id").get("value")

    def default_network(self):
        return self._client.post(
            "/network/add", follow_redirects=True, data=default_network
        )


class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, username="admin", password="password"):
        return self._client.post(
            "/login",
            follow_redirects=True,
            data={"login": username, "password": password},
        )

    def logout(self):
        res = self._client.get("/logout", follow_redirects=True)
        return res


@pytest.fixture(scope="module")
def app_minimal():
    app: Flask = create_app("flask_test.cfg")
    app.config["WTF_CSRF_ENABLED"] = False
    yield app


@pytest.fixture(scope="module")
def app():
    app: Flask = create_app("flask_test.cfg")
    app.config["WTF_CSRF_ENABLED"] = False

    # Return app object and run tests
    yield app


@pytest.fixture(scope="module")
def client(app):
    with app.test_client() as testing_client:
        with app.app_context():
            yield app.test_client()


pytest.fixture(scope="module")


def runner(app):
    with app.test_cli_runner() as testing_client:
        with app.app_context():
            yield app.test_cli_runner()


@pytest.fixture
def auth(client):
    return AuthActions(client)


@pytest.fixture
def networks(client):
    return Networks(client)


# Prepare a fresh data set with networks, clients and alias
@pytest.fixture(scope="function")
def setup_network(client, request):
    rv = client.post(
        "/login",
        follow_redirects=True,
        data={"login": "admin", "password": "password"},
    )

    rv = client.post(
        "/network/delete/{0}".format(default_network["address"]),
        data=dict(address=default_network["address"]),
        follow_redirects=True,
    )
    rv = client.post("/network/add", data=default_network)
    rv = client.get("/host/add/192.168.0.0")
    net_id = network_id(client)
    rv = client.post("/host/add/192.168.0.0", data=default_host(net_id))
    rv = client.get("/host/list/192.168.0.0")
    rv = client.get("/host/edit/192.168.0.10?network_address=192.168.0.0")
    rv = client.post(
        "/alias/add/{0}".format(default_host(net_id)["address"].decode("utf-8")),
        data=dict(name="testalias"),
        follow_redirects=True,
    )
    rv = client.post(
        "/network/delete/{0}".format(temp_network["address"]),
        data=dict(address=temp_network["address"]),
        follow_redirects=True,
    )

    def finalizer():
        rv = client.post(
            "/network/delete/{0}".format(temp_network["address"]),
            data=dict(address=temp_network["address"]),
            follow_redirects=True,
        )
        rv = client.post(
            "/network/delete/{0}".format(default_network["address"]),
            data=dict(address=default_network["address"]),
            follow_redirects=True,
        )

    request.addfinalizer(finalizer)


class UserActions:
    def __init__(self, client):
        self._client = client

    def create(self, login, role, password, confirm):
        self._client.post(
            "/user/add/0",
            follow_redirects=True,
            data={
                "login": login,
                "role": role,
                "password": password,
                "confirm": confirm,
            },
        )
        rv = self._client.get("/user")
        users = pd.read_html(StringIO(rv.data.decode()))[0]
        wanted_user = users[users["Name"] == login]
        return wanted_user["ID"].values[0]

    def delete(self, user_id):
        self._client.post(
            "/user/delete/{0}".format(user_id),
            follow_redirects=True,
            data=dict(id=user_id),
        )


@pytest.fixture
def user(client):
    return UserActions(client)
