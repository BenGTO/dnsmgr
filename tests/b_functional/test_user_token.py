import pytest
from bs4 import BeautifulSoup
from random import randint

from dnsmgr.cryptography.auth_token import token_decode

secret_key = "11111111111111111111111111111111"


def test_token_roundtrip(app, auth, client):
    auth.login()
    rv = client.get("/token_list")

    token_name = "Testtoken_{0}".format(randint(1000000, 9999999))

    rv = client.post(
        "/token/add",
        follow_redirects=True,
        data=dict(name=token_name, decription="what am I", user_id=1, expiration="1h"),
    )

    # Search token on web page
    soup = BeautifulSoup(rv.data, "html.parser")
    token = str(soup.find(id="token").contents[0]).replace("\n", "").strip()
    token_id = str(soup.find(id="id").get("value"))

    # Basic tests
    assert token is not None
    assert len(token) > 150
    rv = client.get("/token_list")
    assert token_name.encode() in rv.data

    # User token for authenticated request
    auth.logout()
    data = token_decode(secret=secret_key, token=token)
    assert data["user_id"] == "1"

    rv = client.get(
        "/network",
        follow_redirects=True,
        headers={"Authorization": "Bearer {0}".format(token)},
    )
    assert rv.status_code == 200

    auth.login()
    rv = client.post("/token/delete/{0}".format(token_id), follow_redirects=True)
    assert rv.status_code == 200
    assert b"Your token list" in rv.data
    assert token_name.encode() not in rv.data


@pytest.mark.parametrize(
    "lifetime",
    ("", "0", "-1h", "a"),
)
def test_token_add_invalid_lifetime(auth, client, lifetime):
    auth.login()

    token_name = "Testtoken_{0}".format(randint(1000000, 9999999))

    rv = client.post(
        "/token/add",
        follow_redirects=True,
        data=dict(
            name=token_name, decription="what am I", user_id=1, expiration=lifetime
        ),
    )
    assert b"Invalid time specification" in rv.data


def test_token_add_duplicate_name(auth, client):
    auth.login()

    rv = client.post(
        "/token/add",
        follow_redirects=True,
        data=dict(name="Duplicate", user_id=1, expiration="1h"),
    )
    rv = client.post(
        "/token/add",
        follow_redirects=True,
        data=dict(name="Duplicate", user_id=1, expiration="1h"),
    )
    assert b"You already have a token of that name" in rv.data


def test_no_header(auth, client):
    auth.logout()
    rv = client.get("/network", follow_redirects=True)
    assert b"Please log in to access this page" in rv.data


def test_no_bearer(auth, client):
    auth.logout()
    rv = client.get(
        "/network",
        follow_redirects=True,
        headers={"Authorization": "Whatever 11111"},
    )
    assert b"Please log in to access this page" in rv.data


def test_empty_auth(auth, client):
    auth.logout()
    rv = client.get(
        "/network",
        follow_redirects=True,
        headers={"Authorization": ""},
    )
    assert b"Please log in to access this page" in rv.data


def test_only_bearer(auth, client):
    auth.logout()
    rv = client.get(
        "/network",
        follow_redirects=True,
        headers={"Authorization": "Bearer"},
    )
    assert b"Please log in to access this page" in rv.data
