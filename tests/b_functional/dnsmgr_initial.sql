-- Adminer 4.8.1 MySQL 11.6.2-MariaDB-ubu2404 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `dnsmgr`;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `alias`;
CREATE TABLE `alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `comment` varchar(4096) DEFAULT NULL,
  `host_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `host_id` (`host_id`),
  CONSTRAINT `alias_ibfk_1` FOREIGN KEY (`host_id`) REFERENCES `host` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_uca1400_ai_ci;


DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(255) NOT NULL,
  `domain_name_server` varchar(2048) NOT NULL,
  `netbios_name_server` varchar(2048) NOT NULL,
  `default_netmask` varchar(255) NOT NULL,
  `default_lease_time` int(11) NOT NULL,
  `max_lease_time` int(11) NOT NULL,
  `shared_network` tinyint(1) NOT NULL,
  `master_dns` varchar(255) NOT NULL,
  `dns_ttl` int(11) NOT NULL,
  `webhook` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_uca1400_ai_ci;


DROP TABLE IF EXISTS `host`;
CREATE TABLE `host` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `network_id` int(11) NOT NULL,
  `mac` varchar(25) DEFAULT NULL,
  `comment` varchar(4096) DEFAULT NULL,
  `addr_sorting` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `address` (`address`),
  UNIQUE KEY `addr_sorting` (`addr_sorting`),
  UNIQUE KEY `mac` (`mac`),
  KEY `network_id` (`network_id`),
  CONSTRAINT `host_ibfk_1` FOREIGN KEY (`network_id`) REFERENCES `network` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_uca1400_ai_ci;

INSERT INTO `host` (`id`, `name`, `address`, `network_id`, `mac`, `comment`, `addr_sorting`) VALUES
(1,	'existinghost',	'192.168.0.20',	3,	'aa:bb:cc:aa:bb:cc',	NULL,	3232235530);

DROP TABLE IF EXISTS `network`;
CREATE TABLE `network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `netmask` varchar(20) NOT NULL,
  `gateway` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `broadcast` varchar(255) NOT NULL,
  `dhcp_pool_start` varchar(255) NOT NULL,
  `dhcp_pool_end` varchar(255) NOT NULL,
  `dhcp_pool_known_clients` tinyint(1) NOT NULL,
  `dhcp_pool_unknown_clients` tinyint(1) NOT NULL,
  `dhcp_dns_server` varchar(1020) NOT NULL,
  `dhcp_netbios_server` varchar(255) DEFAULT NULL,
  `dhcp_default_lease_time` int(11) DEFAULT NULL,
  `dhcp_max_lease_time` int(11) DEFAULT NULL,
  `dhcp_min_secs` int(11) DEFAULT NULL,
  `dhcp_nextserver` varchar(255) DEFAULT NULL,
  `dhcp_filename` varchar(255) DEFAULT NULL,
  `addr_sorting` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `address` (`address`),
  UNIQUE KEY `gateway` (`gateway`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `broadcast` (`broadcast`),
  UNIQUE KEY `addr_sorting` (`addr_sorting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_uca1400_ai_ci;

INSERT INTO `network` (`id`, `address`, `netmask`, `gateway`, `name`, `broadcast`, `dhcp_pool_start`, `dhcp_pool_end`, `dhcp_pool_known_clients`, `dhcp_pool_unknown_clients`, `dhcp_dns_server`, `dhcp_netbios_server`, `dhcp_default_lease_time`, `dhcp_max_lease_time`, `dhcp_min_secs`, `dhcp_nextserver`, `dhcp_filename`, `addr_sorting`) VALUES
(1,	'192.168.0.0',	'255.255.255.128',	'192.168.0.1',	'existingnet',	'192.168.0.255',	'192.168.0.5',	'192.168.0.10',	1,	1,	'192.168.0.200',	'192.168.0.200',	600,	1800,	10,	'bootserver',	'bootfile',	3232235520);

INSERT INTO `users` (`id`, `login`, `hashed_password`, `authenticated`, `registered_on`, `role`, `design`) VALUES
(1,	'admin',	'$2b$12$YFii8lhApXPYdVKvlAZWxOaGlCMLFZ4adYMhCt/Wr2KcGbTmIHP8e',	1,	'2025-01-25 15:47:14',	'user',	'default');

-- 2025-01-25 14:58:40
