from bs4 import BeautifulSoup

# OK networks

common_network = dict(
    netmask="255.255.255.0",
    broadcast="192.168.0.255",
    gateway="192.168.0.1",
    dhcp_default_lease_time="600",
    dhcp_max_lease_time="1800",
    dhcp_dns_server="192.168.0.200",
    dhcp_netbios_server="192.168.0.200",
    dhcp_nextserver="bootserver",
    dhcp_filename="bootfile",
    dhcp_pool_known_clients="True",
    dhcp_pool_unknown_clients="False",
    dhcp_min_secs="10",
)
default_network = common_network.copy()
default_network.update(
    dict(
        name=b"testnet",
        address="192.168.0.0",
        dhcp_pool_start="192.168.0.5",
        dhcp_pool_end="192.168.0.10",
    )
)

temp_network = common_network.copy()
temp_network.update(
    dict(
        name=b"tempnet",
        address="192.168.20.0",
        broadcast="192.168.20.255",
        gateway="192.168.20.1",
    )
)

# FAIL networks

same_address_network = default_network.copy()
same_address_network.update(dict(name="badnet"))


def network_id(client):
    rv = client.get("/host/add/192.168.0.0")
    soup = BeautifulSoup(rv.data, "html.parser")
    return soup.find(id="network_id").get("value")


# OK hosts
def default_host(net_id):
    return dict(
        name=b"testhost",
        address=b"192.168.0.10",
        mac="aa:bb:cc:aa:bb:cc",
        network_id=net_id,
    )


def other_host(net_id):
    return dict(
        name=b"otherhost",
        address=b"192.168.0.11",
        mac="aa:bb:cc:dd:ee:ff",
        network_id=net_id,
    )


# FAIL hosts
def same_mac_host(net_id):
    return dict(
        name=b"badmachost",
        address=b"192.168.0.20",
        mac="aa:bb:cc:aa:bb:cc",
        network_id=net_id,
    )


def bad_mac_host(net_id):
    return dict(
        name=b"badmachost",
        address=b"192.168.0.20",
        mac="aa:bb:cc:aa:bb:xy",
        network_id=net_id,
    )


def no_mac_host(net_id):
    return dict(name=b"nomachost", address=b"192.168.0.30", network_id=net_id)


def no_mac_host2(net_id):
    return dict(name=b"nomachost2", address=b"192.168.0.31", network_id=net_id)


def same_name_host(net_id):
    return dict(
        name=b"testhost",
        address=b"192.168.0.20",
        mac="aa:bb:cc:ff:ff:ff",
        network_id=net_id,
    )


def same_address_host(net_id):
    return dict(
        name=b"badaddresshost",
        address=b"192.168.0.10",
        mac="aa:bb:cc:ff:ff:ff",
        network_id=net_id,
    )


def bad_subnet_host(net_id):
    return dict(
        name=b"badaddresshost",
        address=b"192.168.1.10",
        mac="aa:bb:cc:ff:ff:ff",
        network_id=net_id,
    )


# Config
default_conf = dict(
    domain_name="example.com",
    domain_name_server="192.168.111.111",
    netbios_name_server="192.168.111.111",
    default_netmask="255.255.255.0",
    default_lease_time="600",
    max_lease_time="3600",
    master_dns="ns1",
    dhcp_pool_start="192.168.0.100",
    dhcp_pool_end="192.168.0.200",
    dns_ttl="1800",
    webhook="https://google.com",
)
