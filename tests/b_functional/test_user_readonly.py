testuser = {"login": "janedoe", "password": "123456", "role": "readonly"}


def test_readonly_alias(auth, client, user, setup_network):
    auth.login()
    user.create(
        testuser["login"], testuser["role"], testuser["password"], testuser["password"]
    )
    auth.logout()

    auth.login(testuser["login"], testuser["password"])
    rv = client.post(
        "/alias/add/192.168.0.10",
        follow_redirects=True,
        data=dict(name="readonly-alias"),
    )
    assert b"You only have read access" in rv.data
    rv = client.get("/alias/list/192.168.0.10")
    assert b"readonly-alias" not in rv.data

    rv = client.post(
        "/alias/edit/testalias", follow_redirects=True, data=dict(name="changedalias")
    )
    assert b"You only have read access" in rv.data
    rv = client.get("/alias/list/192.168.0.10")
    assert b"changedalias" not in rv.data

    rv = client.post("/alias/delete/testalias", follow_redirects=True)
    assert b"You only have read access" in rv.data
    rv = client.get("/alias/list/192.168.0.10")
    assert b"testalias" in rv.data
    auth.logout()


def test_readonly_config(auth, client, user, setup_network):
    auth.login()
    user_id = user.create(
        testuser["login"], testuser["role"], testuser["password"], testuser["password"]
    )
    auth.logout()
    auth.login(testuser["login"], testuser["password"])

    rv = client.get("/config")
    assert b"master_dns" in rv.data

    rv = client.post("/config", follow_redirects=True)
    assert b"You only have read access" in rv.data
    auth.logout()
    auth.login()
    client.post("/user/delete/{0}".format(user_id))


def test_readonly_host(auth, client, user, setup_network):
    auth.login()
    user_id = user.create(
        testuser["login"], testuser["role"], testuser["password"], testuser["password"]
    )
    auth.logout()
    auth.login(testuser["login"], testuser["password"])

    rv = client.post(
        "/host/add/192.168.0.0",
        follow_redirects=True,
        data=dict(name="readonly-host", address="192.168.0.100"),
    )
    assert b"You only have read access" in rv.data
    rv = client.get("/host/list/192.168.0.0")
    assert b"readonly-host" not in rv.data

    rv = client.get("/host/edit/192.168.0.10?network_address=192.168.0.0")
    assert b"testhost" in rv.data
    assert b"Edit Host" in rv.data
    rv = client.post(
        "/host/edit/192.168.0.10",
        follow_redirects=True,
        data=dict(name="readonly-host"),
    )
    assert b"You only have read access" in rv.data
    rv = client.get("/host/list/192.168.0.0")
    assert b"readonly-host" not in rv.data

    rv = client.post("/host/delete/192.168.10", follow_redirects=True)
    assert b"You only have read access" in rv.data
    rv = client.get("/host/list/192.168.0.0")
    assert b"testhost" in rv.data

    auth.logout()
    auth.login()
    client.post("/user/delete/{0}".format(user_id))


def test_readonly_network(auth, client, user, setup_network):
    auth.login()
    user_id = user.create(
        testuser["login"], testuser["role"], testuser["password"], testuser["password"]
    )
    auth.logout()
    auth.login(testuser["login"], testuser["password"])

    rv = client.post(
        "/network/add",
        follow_redirects=True,
        data=dict(name="readonly-network", address="192.168.100.0"),
    )
    assert b"You only have read access" in rv.data
    rv = client.get("/network")
    assert b"readonly-network" not in rv.data

    rv = client.get("/network/edit/192.168.0.0")
    assert b"testnet" in rv.data
    assert b"Edit Network" in rv.data
    rv = client.post(
        "/network/edit/192.168.0.10",
        follow_redirects=True,
        data=dict(name="readonly-network"),
    )
    assert b"You only have read access" in rv.data
    rv = client.get("/network")
    assert b"readonly-network" not in rv.data

    rv = client.post("/network/delete/192.168.10", follow_redirects=True)
    assert b"You only have read access" in rv.data
    rv = client.get("/host/list/192.168.0.0")
    assert b"testhost" in rv.data

    auth.logout()
    auth.login()
    client.post("/user/delete/{0}".format(user_id))
