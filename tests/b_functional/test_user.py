import pandas as pd
from io import StringIO

user = {
    "login": "janedoe",
    "password": "123456",
}


def get_user_id(client, name):
    rv = client.get("/user", follow_redirects=True)
    users = pd.read_html(StringIO(rv.data.decode()))[0]
    wanted_user = users[users["Name"] == name]
    return wanted_user["ID"].values[0]


def test_user_creation(auth, client):
    auth.login()

    rv = client.post(
        "/user/add/0",
        follow_redirects=True,
        data=dict(
            login=user["login"], password=user["password"], confirm=user["password"]
        ),
    )

    assert b"User added" in rv.data
    assert user["login"].encode() in rv.data

    rv = client.get("/user")
    user_id = get_user_id(client, user["login"])

    # Try to add again
    rv = client.post(
        "/user/add/0",
        follow_redirects=True,
        data=dict(
            login=user["login"], password=user["password"], confirm=user["password"]
        ),
    )

    assert b"Duplicate name" in rv.data

    rv = client.get("/user/delete/{0}".format(user_id))
    assert "Delete user {0}".format(user["login"]).encode() in rv.data

    rv = client.post(
        "/user/delete/{0}".format(user_id), follow_redirects=True, data=dict(id=user_id)
    )

    assert b"Networks" in rv.data
    assert user["login"].encode() not in rv.data


def test_bad_user_creation(auth, client):
    auth.login()

    # name with whitespace
    rv = client.post(
        "/user/add/0",
        follow_redirects=True,
        data=dict(
            login="jane doe", password=user["password"], confirm=user["password"]
        ),
    )
    assert (
        b"Login names may not contain whitespace" in rv.data and b"Add User" in rv.data
    )

    # password don't match
    rv = client.post(
        "/user/add/0",
        follow_redirects=True,
        data=dict(login="jane doe", password=user["password"], confirm="differs"),
    )
    assert b"Field must be equal to password" in rv.data and b"Add User" in rv.data


def test_unknown_user_deletion(auth, client):
    auth.login()
    rv = client.post("/user/delete/99999", follow_redirects=True, data=dict(id=99999))
    assert b"Attempted to delete a non existent user" in rv.data


def test_unauthorized_user(auth, client):
    auth.logout()
    rv = client.get("/user", follow_redirects=True)
    assert b"Please log in" in rv.data
    rv = client.get("/user/add/0", follow_redirects=True)
    assert b"Please log in" in rv.data
    rv = client.get("/user/edit/0", follow_redirects=True)
    assert b"Please log in" in rv.data
    rv = client.get("/user/delete/0", follow_redirects=True)
    assert b"Please log in" in rv.data

    auth.login()
    client.post(
        "/user/add/0",
        follow_redirects=True,
        data=dict(
            login=user["login"], password=user["password"], confirm=user["password"]
        ),
    )
    user_id = get_user_id(client, user["login"])
    auth.logout()

    auth.login(username=user["login"], password=user["password"])
    rv = client.get("/user", follow_redirects=True)
    assert b"You must be admin" in rv.data
    rv = client.get("/user/add/0", follow_redirects=True)
    assert b"You must be admin" in rv.data
    rv = client.get("/user/edit/0", follow_redirects=True)
    assert b"You must be admin" in rv.data
    rv = client.get("/user/delete/0", follow_redirects=True)
    assert b"You must be admin" in rv.data

    auth.logout()
    auth.login()
    client.post("/user/delete/{0}".format(user_id), data=dict(id=user_id))


def test_change_name(auth, client):
    auth.login()
    rv = client.post(
        "/user/add/0",
        follow_redirects=True,
        data=dict(
            login=user["login"], password=user["password"], confirm=user["password"]
        ),
    )
    user_id = get_user_id(client, user["login"])

    rv = client.post(
        "/user/edit/{0}".format(user_id),
        follow_redirects=True,
        data=dict(id=user_id, login="newname", password="", confirm=""),
    )
    assert b"Users" in rv.data
    assert b"newname" in rv.data

    client.post("/user/delete/{0}".format(user_id), data=dict(id=user_id))


def test_change_role(auth, client):
    auth.login()
    client.post(
        "/user/add/0",
        follow_redirects=True,
        data=dict(
            login=user["login"], password=user["password"], confirm=user["password"]
        ),
    )
    user_id = get_user_id(client, user["login"])

    rv = client.post(
        "/user/edit/{0}".format(user_id),
        follow_redirects=True,
        data=dict(id=user_id, role="readonly"),
    )
    assert b"Users" in rv.data
    assert b"readonly" in rv.data

    client.post("/user/delete/{0}".format(user_id), data=dict(id=user_id))


def test_change_password(auth, client):
    # Password changes by admins in user management
    #
    # Acting as admin: create test user
    auth.login()
    client.post(
        "/user/add/0",
        follow_redirects=True,
        data=dict(
            login=user["login"], password=user["password"], confirm=user["password"]
        ),
    )
    user_id = get_user_id(client, user["login"])

    # Password and confirmation differ
    rv = client.post(
        "/user/edit/{0}".format(user_id),
        follow_redirects=True,
        data=dict(id=user_id, password="abc", confirm="efg"),
    )
    assert b"Field must be equal to password" in rv.data
    # Password with whitespace
    rv = client.post(
        "/user/edit/{0}".format(user_id),
        follow_redirects=True,
        data=dict(id=user_id, password="abc abc"),
    )
    assert b"Passwords may not contain whitespace" in rv.data

    # Set valid password
    rv = client.post(
        "/user/edit/{0}".format(user_id),
        follow_redirects=True,
        data=dict(id=user_id, password="abcabc", confirm="abcabc"),
    )
    assert b"New password set" in rv.data
    auth.logout()
    # Acting as test user, using the new password
    rv = auth.login(user["login"], "abcabc")
    assert b"Thanks for logging in" in rv.data
    auth.logout()
    auth.login()
    client.post("/user/delete/{0}".format(user_id), data=dict(id=user_id))
