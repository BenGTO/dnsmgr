def set_profile(client, design="default"):
    return client.post("/profile", data=dict(design=design), follow_redirects=True)


def test_settings_dark_design(auth, client):
    auth.login()
    rv = set_profile(client, "dark")
    assert b"User Profile" in rv.data
    assert (
        b'input checked id="design-1" name="design" type="radio" value="dark"'
        in rv.data
    )
    assert b"slate/bootstrap" in rv.data


def test_settings_default_design(auth, client):
    auth.login()
    rv = set_profile(client, "default")
    assert b"User Profile" in rv.data
    assert (
        b'input checked id="design-0" name="design" type="radio" value="default"'
        in rv.data
    )
    assert b"css/bootstrap" in rv.data
