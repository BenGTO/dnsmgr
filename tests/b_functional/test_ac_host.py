from bs4 import BeautifulSoup
from tests.b_functional.testitems import (
    default_host,
    other_host,
    same_address_host,
    same_mac_host,
    bad_mac_host,
    same_name_host,
    bad_subnet_host,
    no_mac_host,
    no_mac_host2,
)


def network_id(client):
    rv = client.get("/host/add/192.168.0.0")
    soup = BeautifulSoup(rv.data, "html.parser")
    return soup.find(id="network_id").get("value")


def host_add(client, data):
    return client.post("/host/add/192.168.0.0", data=data, follow_redirects=True)


def host_edit(client, data):
    return client.post(
        "/host/edit/192.168.0.10?network_address=192.168.0.0",
        data=data,
        follow_redirects=True,
    )


def host_delete(client, ip):
    return client.post(
        "/host/delete/{0}".format(ip), data=dict(address=ip), follow_redirects=True
    )


# Actual tests
# Good host
def test_host_add(auth, networks, client, setup_network):
    auth.login()
    net_id = network_id(client)
    rv = host_add(client, default_host(net_id))
    assert default_host(net_id)["name"] in rv.data


def test_host_no_mac_add(auth, client, setup_network):
    auth.login()
    net_id = network_id(client)
    host_add(client, no_mac_host(net_id))
    rv = host_add(client, no_mac_host2(net_id))
    assert (
        no_mac_host(net_id)["name"] in rv.data
        and no_mac_host2(net_id)["name"] in rv.data
    )


# Bad hosts
def test_host_add_duplicates(auth, client, setup_network):
    auth.login()
    net_id = network_id(client)
    host_add(client, default_host(net_id))

    rv = host_add(client, same_name_host(net_id))
    assert b"Duplicate name" in rv.data
    rv = host_add(client, same_address_host(net_id))
    assert b"Duplicate address" in rv.data
    rv = host_add(client, same_mac_host(net_id))
    assert b"Duplicate MAC" in rv.data


def test_host_add_invalid(auth, client, setup_network):
    auth.login()
    net_id = network_id(client)

    rv = host_add(client, bad_subnet_host(net_id))
    assert b"Address not in subnet range" in rv.data

    rv = host_add(client, bad_mac_host(net_id))
    assert b"Not a valid MAC address" in rv.data

    host = other_host(net_id)
    host["name"] = "other host"
    rv = host_add(client, host)
    assert b"Invalid character(s) detected" in rv.data

    rv = host_add(client, no_mac_host(net_id))
    assert no_mac_host(net_id)["name"] in rv.data


def test_host_edit(auth, client, setup_network):
    auth.login()
    net_id = network_id(client)

    # Edit host tests
    rv = client.get("/host/edit/192.168.0.10?network_address=192.168.0.0")
    assert default_host(net_id)["name"] in rv.data

    host = default_host(net_id)
    host["name"] = "changedhost"
    host["network_id"] = net_id
    rv = host_edit(client, host)
    assert b"Hosts in network" in rv.data and b"changedhost" in rv.data

    host = default_host(0)
    host["address"] = "192.168.0.21"
    host["network_id"] = net_id
    rv = host_edit(client, host)
    assert b"Hosts in network" in rv.data and b"192.168.0.21" in rv.data


def test_host_edit_duplicates(auth, client, networks, setup_network):
    auth.login()
    net_id = network_id(client)
    host_add(client, other_host(net_id))
    host = default_host(net_id)
    host_add(client, host)

    rv = client.get("/network")
    rv = client.get("/host/list/192.168.0.0")

    host["name"] = other_host(net_id)["name"]
    rv = host_edit(client, host)
    assert b"Duplicate name" in rv.data

    host = default_host(net_id)
    host["address"] = other_host(net_id)["address"]
    rv = host_edit(client, host)
    assert b"Duplicate address" in rv.data

    host = default_host(net_id)
    host["mac"] = other_host(net_id)["mac"]
    rv = host_edit(client, host)
    assert b"Duplicate MAC" in rv.data

    # also test alias
    client.post(
        "/alias/add/{0}".format(host["address"].decode("utf-8")),
        data=dict(name="testname"),
        follow_redirects=True,
    )
    host = other_host(net_id)
    host["name"] = "testname"
    rv = host_edit(client, host)
    assert b"Duplicate name" in rv.data


def test_host_delete(auth, client, setup_network):
    auth.login()
    # networks.default_network()
    net_id = network_id(client)
    host_add(client, default_host(net_id))

    rv = host_delete(client, "192.168.0.10")
    assert default_host(net_id)["name"] not in rv.data

    rv = host_delete(client, "192.168.0.10")
    assert b"Attempted to delete a non existent host" in rv.data
