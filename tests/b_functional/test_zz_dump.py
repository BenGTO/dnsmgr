from testitems import default_conf


def test_output_dhcp(app, auth, client, setup_network):
    auth.login()
    client.post("/config", data=default_conf)

    app.config["RESTRICT_ACCESS_TO_DUMPS"] = False
    auth.logout()
    rv = client.get("/network/dhcpd.conf", follow_redirects=True)
    assert b'option domain-name "example.com"' in rv.data
    assert b"option domain-name-servers 192.168.111.111" in rv.data
    assert b"option subnet-mask 255.255.255.0" in rv.data
    assert b"default-lease-time 600" in rv.data
    assert b"subnet 192.168.0.0 netmask 255.255.255.0 {" in rv.data
    assert b"option routers 192.168.0.1" in rv.data
    assert b"option subnet-mask 255.255.255.0" in rv.data
    assert b"option broadcast-address 192.168.0.255" in rv.data
    assert b"range 192.168.0.5 192.168.0.10" in rv.data
    assert b"host testhost" in rv.data
    assert b"hardware ethernet aa:bb:cc:aa:bb:cc" in rv.data
    assert b"fixed-address 192.168.0.10;" in rv.data

    app.config["RESTRICT_ACCESS_TO_DUMPS"] = True
    rv = client.get("/network/dhcpd.conf", follow_redirects=True)
    assert rv.status_code == 403


def test_output_dns(app, auth, client, setup_network):
    auth.login()
    client.post("/config", data=default_conf)

    app.config["RESTRICT_ACCESS_TO_DUMPS"] = False
    auth.logout()
    rv = client.get("/network/db.site", follow_redirects=True)
    assert b"testhost IN A 192.168.0.10" in rv.data

    app.config["RESTRICT_ACCESS_TO_DUMPS"] = True
    rv = client.get("/network/db.site", follow_redirects=True)
    assert rv.status_code == 403


def test_output_dns_rev(app, auth, client, setup_network):
    auth.login()
    client.post("/config", data=default_conf)

    app.config["RESTRICT_ACCESS_TO_DUMPS"] = False
    auth.logout()
    rv = client.get("/network/db.rev/192.168.0.0", follow_redirects=True)
    assert b"IN PTR testhost.example.com" in rv.data

    app.config["RESTRICT_ACCESS_TO_DUMPS"] = True
    rv = client.get("/network/db.rev/192.168.0.0", follow_redirects=True)
    assert rv.status_code == 403
