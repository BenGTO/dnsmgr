def test_searchpage(auth, client):
    auth.login()
    rv = client.get("/search")
    assert b"Search System" in rv.data


def test_search(auth, client, setup_network):
    auth.login()

    rv = client.post("/search", data=dict(searchpattern="testhos"))
    assert b"testhost" in rv.data


def test_search_alias(auth, client, setup_network):
    auth.login()
    client.post(
        "/alias/add/192.168.0.10", data=dict(name="testalias"), follow_redirects=True
    )
    rv = client.post("/search", data=dict(searchpattern="testalia"))
    assert b"testhost" in rv.data
