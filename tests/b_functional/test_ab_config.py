required_data = dict(
    domain_name_server="127.0.0.1",
    netbios_name_server="ns1",
    default_netmask="255.255.255.0",
    default_lease_time="600",
    max_lease_time="6000",
    master_dns="ns1",
    dns_ttl="600",
)


def test_config(auth, client):
    auth.login()
    rv = client.get("/config")
    assert b"Configuration" in rv.data

    host = required_data.copy()
    host["domain_name"] = "example.org"
    rv = client.post("/config", data=host)
    assert b"example.org" in rv.data
    host["domain_name"] = "example.com"
    rv = client.post("/config", data=host)
    assert b"example.com" in rv.data
