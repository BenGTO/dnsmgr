from tests.b_functional.testitems import (
    default_network,
    same_address_network,
    temp_network,
)


def test_network_add_empty(auth, client):
    # Fall back to add form
    auth.login()
    response = client.post("/network/add", follow_redirects=True, data=dict())
    assert b"Add a Network" in response.data


def test_network_add(auth, client, setup_network):
    auth.login()
    rv = client.get("/network")
    # Add existing network again
    response = client.post("/network/add", follow_redirects=True, data=default_network)
    # Duplicate refused?
    assert b"Duplicate name" in response.data

    # Add new network
    response = client.post("/network/add", follow_redirects=True, data=temp_network)
    # Network in results list?
    assert b"Network Overview" in response.data
    assert temp_network["name"] in response.data
    # Add network with same IP
    response = client.post(
        "/network/add", follow_redirects=True, data=same_address_network
    )
    # Duplicate refused?
    assert b"Duplicate address" in response.data

    # Network host list works?
    response = client.get(
        "/host/list/{0}".format(temp_network["address"]), follow_redirects=True
    )
    assert (
        b"Hosts in network" in response.data and temp_network["name"] in response.data
    )

    # Delete network
    response = client.post(
        "/network/delete/{0}".format(temp_network["address"]),
        data=dict(address=temp_network["address"]),
        follow_redirects=True,
    )
    assert temp_network["name"] not in response.data


def test_network_edit(auth, networks, client, setup_network):
    auth.login()
    client.post("/network/add", follow_redirects=True, data=temp_network)
    # Edit nonexistent network
    response = client.get("/network/edit/1.1.1.1", follow_redirects=True)
    # Not found?
    assert b"Network not found" in response.data
    # Add and show network edit mask
    response = client.post("/network/add", follow_redirects=True, data=temp_network)
    response = client.get(
        "/network/edit/{0}".format(temp_network["address"]), follow_redirects=True
    )
    # Network in edit form?
    assert b"Edit Network" in response.data and temp_network["name"] in response.data
    # Modify network input data
    data_new = temp_network.copy()
    data_new["netmask"] = "255.255.255.128"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"255.255.255.128" in response.data and b"Network Overview" in response.data
    # Whitespace detection
    data_new = temp_network.copy()
    data_new["name"] = "new net"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Whitespace detected" in response.data

    # bad IPs
    data_new = temp_network.copy()
    data_new["address"] = "abcdefgh"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Invalid address" in response.data
    data_new = temp_network.copy()
    data_new["netmask"] = "abcdefgh"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Invalid address" in response.data
    data_new = temp_network.copy()
    data_new["gateway"] = "abcdefgh"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Invalid address" in response.data
    data_new = temp_network.copy()
    data_new["gateway"] = "192.168.1.0"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Gateway outside network range" in response.data
    data_new = temp_network.copy()
    data_new["dhcp_pool_start"] = "192.168.1.2"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Range start outside network range" in response.data
    data_new = temp_network.copy()
    data_new["dhcp_pool_end"] = "192.168.1.200"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Range end outside network range" in response.data
    data_new = temp_network.copy()
    data_new["dhcp_pool_start"] = "192.168.20.6"
    data_new["dhcp_pool_end"] = "192.168.20.5"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"End of range must be higher than start" in response.data
    data_new = temp_network.copy()
    data_new["dhcp_pool_start"] = "192.168.20.2"
    data_new["dhcp_pool_end"] = ""
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"End mandatory if start is set" in response.data
    data_new = temp_network.copy()
    data_new["dhcp_pool_start"] = ""
    data_new["dhcp_pool_end"] = "192.168.20.2"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Start mandatory if end is set" in response.data
    data_new = temp_network.copy()
    data_new["dhcp_dns_server"] = "_+4"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Invalid address" in response.data
    data_new = temp_network.copy()
    data_new["dhcp_netbios_server"] = "_+4,403f"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"Invalid address" in response.data

    # Other No-Nos
    data_new = temp_network.copy()
    data_new["dhcp_max_lease_time"] = "4"
    data_new["dhcp_default_lease_time"] = "9"
    response = client.post(
        "/network/edit/{0}".format(temp_network["address"]),
        data=data_new,
        follow_redirects=True,
    )
    assert b"ust be longer than default value" in response.data

    # Cleanup
    response = client.post(
        "/network/delete/{0}".format(temp_network["address"]),
        data=dict(address=temp_network["address"]),
        follow_redirects=True,
    )
