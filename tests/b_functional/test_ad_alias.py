from bs4 import BeautifulSoup

from tests.b_functional.testitems import (
    default_host,
    default_network,
    other_host,
    network_id,
)


def host_id(client):
    rv = client.get("/alias/edit/probealias?host_address=192.168.0.11")
    soup = BeautifulSoup(rv.data, "html.parser")
    return soup.find(id="host_id").get("value")


def host_add(client, data):
    return client.post("/host/add/192.168.0.0", data=data, follow_redirects=True)


def populated_network(auth, client):
    auth.login()
    client.post("/network/add", follow_redirects=True, data=default_network)
    net_id = network_id(client)
    host_add(client, default_host(net_id))
    host_add(client, other_host(net_id))


def test_alias_add(auth, setup_network, client):
    auth.login()
    rv = client.post(
        "/alias/add/192.168.0.10", data=dict(name="newalias"), follow_redirects=True
    )
    assert b"newalias" in rv.data and b"Aliases for" in rv.data


def test_alias_subdomain_add(auth, setup_network, client):
    auth.login()
    rv = client.post(
        "/alias/add/192.168.0.10", data=dict(name="newalias.sub"), follow_redirects=True
    )
    assert b"newalias.sub" in rv.data and b"Aliases for" in rv.data


def test_alias_add_duplicates(auth, setup_network, client):
    auth.login()
    # to same host
    client.post(
        "/alias/add/192.168.0.10", data=dict(name="testalias"), follow_redirects=True
    )

    # to other host
    client.post("/host/add/192.168.0.0", data=other_host(network_id(client)))
    rv = client.post(
        "/alias/add/192.168.0.11", data=dict(name="testalias"), follow_redirects=True
    )
    assert b"Name already exists as alias" in rv.data
    rv = client.post(
        "/alias/add/192.168.0.11", data=dict(name="testhost"), follow_redirects=True
    )
    assert b"Name already exists as host" in rv.data


def test_alias_add_invalid(auth, setup_network, client):
    auth.login()
    rv = client.post(
        "/alias/add/192.168.0.10", data=dict(name="test alias"), follow_redirects=True
    )
    assert b"Invalid character(s) detected" in rv.data
    rv = client.post(
        "/alias/add/192.168.0.10", data=dict(name="2test"), follow_redirects=True
    )
    assert b"Invalid character(s) detected" in rv.data
    rv = client.post(
        "/alias/add/192.168.0.10", data=dict(name="-test"), follow_redirects=True
    )
    assert b"Invalid character(s) detected" in rv.data
    rv = client.post(
        "/alias/add/192.168.0.10", data=dict(name="te_st"), follow_redirects=True
    )
    assert b"Invalid character(s) detected" in rv.data


def test_alias_edit(auth, setup_network, client):
    auth.login()
    rv = client.post(
        "/alias/add/192.168.0.10", data=dict(name="probealias"), follow_redirects=True
    )
    rv = client.get("/alias/edit/probealias?host_address=192.168.0.10")
    assert b"probealias" in rv.data and b"Edit alias" in rv.data
    rv = client.post(
        "/alias/edit/probealias?host_address=192.168.0.10",
        data=dict(host_id=host_id(client), name="newalias"),
        follow_redirects=True,
    )
    assert b"newalias" in rv.data and b"Aliases for" in rv.data


def test_alias_edit_duplicate(auth, setup_network, client):
    auth.login()
    rv = client.post(
        "/alias/add/192.168.0.10", data=dict(name="testalias"), follow_redirects=True
    )
    assert b"testalias" in rv.data

    rv = client.post(
        "/alias/add/192.168.0.10", data=dict(name="testalias"), follow_redirects=True
    )
    assert b"Alias already exists" in rv.data

    client.post("/host/add/192.168.0.0", data=other_host(network_id(client)))
    rv = client.post(
        "/alias/add/192.168.0.11", data=dict(name="testalias"), follow_redirects=True
    )
    assert b"Name already exists" in rv.data

    rv = client.post("/alias/delete/testalias", data=dict(name="testalias"))
    assert b"probealias" not in rv.data
