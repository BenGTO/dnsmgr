from tests.b_functional.testitems import default_conf


def test_webhook_no_url(auth, client):
    auth.login()
    conf = default_conf
    conf["webhook"] = ""
    client.post("/config", data=conf)
    rv = client.get("/webhook")
    assert b"ERROR: URL incomplete" in rv.data


def test_webhook_bad_url(auth, client):
    auth.login()
    conf = default_conf
    conf["webhook"] = "https://willnotfail.again.overandout"
    client.post("/config", data=conf)
    rv = client.get("/webhook")
    assert b"Connection error" in rv.data


def test_webhook_unauthorized(auth, client):
    auth.login()
    conf = default_conf
    conf["webhook"] = "https://gitlab.com/BenGTO/dnsmgr/edit"
    client.post("/config", data=conf)
    rv = client.get("/webhook")
    assert b"ERROR: got return code 403" in rv.data
