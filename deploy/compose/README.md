Docker Compose
==============

Deploys a MariaDB server, dnsmgr, and a push service.

The push service can be used in dnsmgr to pull a git repository with an Ansible playbook,
and execute a playbook. The play should pull the configuration files from dnsmgr and configure
your DNS and DHCP server.

For the webhook to work, you have to supply env variables:

 - `REPO_URL`: the git repository must contain your playbook and the role(s)
 necessary to configure your servers.
 - `RUN`: ansible command as entered on a command line, i.e. `ansible-playbook -t update update_ip.yml`.
 Don't forget to add whatever you need from your `.ansible.cfg` to a configuration
 locally to the repository.
 - `ANSIBLE_SSH_KEY`: SSH key without passphrase. Create it exclusively for your DNS/DHCP
 servers Ansible has to contact.

In dnsmgr, enter "http://ansible-pusher:5000" as webhook.
