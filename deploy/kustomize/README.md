Kustomizable Kubernetes Installation
====================================

Example in production:

1. [Base configuration](https://gitlab.com/BenGTO/k8s-apps-base/-/tree/master/dnsmgr?ref_type=heads)
2. [Deployment derived](https://gitlab.com/BenGTO/kube-02/-/tree/main/k8s-apps/dnsmgr?ref_type=heads) from Template
