Ansible Container for Push Jobs
===============================

Simple container to run an Ansible playbook.

See this
[https://gitlab.com/BenGTO/k8s-apps-prod/-/tree/master/prod/dnsmgr/pusher](production example)
as reference.
