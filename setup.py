from setuptools import find_packages, setup

with open(".version.txt") as version_file:
    version = version_file.read().strip()

setup(
    name="dnsmgr",
    version=version,
    author="Bengt Giger",
    author_email="beni@directbox.com",
    description="Simple DNS/DHCP manager for home use",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Framework :: Flask",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Topic :: System :: Systems Administration",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.9",
    url="https://gitlab.com/BenGTO/dnsmgr",
    install_requires=[
        "duration-parser",
        "flask",
        "Flask-SQLAlchemy",
        "Flask-Bcrypt",
        "Flask-WTF",
        "Flask-Migrate",
        "Flask-Login",
        "pyjwt",
        "psycopg2-binary",
        "requests",
    ],
)
