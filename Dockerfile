FROM python:3.13
ARG VCS_TAG

LABEL maintainer="Bengt Giger <bengt.giger@alumni.ethz.ch>"

RUN useradd -ms /bin/bash dnsmgr
COPY dnsmgr /app/dnsmgr/
RUN mkdir /app/instance/ && chown dnsmgr:dnsmgr /app/instance/
COPY dnsmgr/helpers /app/helpers/
COPY requirements.txt /app/
WORKDIR /app
RUN echo $VCS_TAG >.version.txt
RUN pip install -r requirements.txt

EXPOSE 4080/tcp
USER dnsmgr
ENV FLASK_APP=dnsmgr
ENV USER=dnsmgr
#CMD flask db upgrade && waitress-serve --port 4080 --call dnsmgr:create_app
CMD flask db upgrade -d /app/dnsmgr/migrations && flask run --host=0.0.0.0 --port=4080
