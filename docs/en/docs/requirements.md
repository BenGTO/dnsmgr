Requirements
============

Installation from Package
-------------------------
Versions 1.4.x have been tested with Python 3.6 - 3.12, while version 1.5.x and later have been tested
with Python 3.9 - 3.13.

Docker Container
----------------
Requires a container runtime like _docker_ or _podman_.
_dnsmgr_ runs fine in Kubernetes. And you need to have a _compose_ tool installed,
like `docker-compose`.

Database Server
---------------
For testing purposes, _dnsmgr_ can make use of a SQlite database file. For
longterm use, a MariaDB/MySQL database server is strongly recommended.

Kubernetes
----------
No special requirements here. If you want to use the SQlite database (which is
not recommended long term), don't use NFS, or use it with care. NFS and
SQlite tend to lead to database file corruption.
