# Installation with Docker

A basic compose file which launches _dnsmgr_, a MariaDB server and a container with
webhook to launch the installed Ansible can be found in the
[project git repository](itlab.com/BenGTO/dnsmgr/-/tree/master/deploy/compose).

The docker image handles automatic upgrades of the database schema.
