# Dump Configuration Files

## ISC DHCP Configuration

Use a browser or curl/wget to read the URL http://your-server/network/dhcpd.conf. The
response can directly be copied to the configuration directory of the ISC dhcpd:

```shell
# Debian
cd /var/dhcp
wget http://your-server/network/dhcpd.conf
```

## Bind DNS Configuration
_dnsmgr_ provides one configuration file for the name resolution, and one for
every subnet for reverse lookups. All files have to be included into the
named configuration.

Let's look at a site with two networks. The first file is here:
http://your-server/network/db.site. The reverse lookup files have the network
address in the URL, like http://your-server/network/db.rev/192.168.0.0

Save the files with `wget` as before and add them to `name.conf.local`:

```
zone "mydomain" {
        type master;
        file "/etc/bind/db.site";
};

zone "0.168.192.in-addr.arpa" {
        type master;
        file "/etc/bind/192.168.0.0";
};

zone "5.168.192.in-addr.arpa" {
        type master;
        file "/etc/bind/192.168.5.0";
};
```
