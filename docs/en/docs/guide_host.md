# Add a Host
Click on the IP address of a [network](guide_network.md) to get to the host list.

A host has 4 attributes:

 - Name
 - Address
 - MAC
 - Comment field

Click on _Add host_ in the upper right. Upon successful submission, you will see
the host in the list. Click on the address to add an alias.

Duplicate entries for host names or MAC addresses will be refused, and more inputs
are validated before being accepted.
