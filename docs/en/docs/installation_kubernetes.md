# Installation in Kubernetes

My deployments are usually maintained with ArgoCD, and kustomize fits
better to ArgoCD than Helm. No helm chart, sorry, but feel free to contribute.

The docker image handles automatic upgrades of the database schema.

## Base Template
Kustomize works with configuration layers. A base layer provides the basic skeleton
for a service, and added layer create the final setup.

A base template can be found in
[my template repository](https://gitlab.com/BenGTO/k8s-apps-base/-/tree/master/dnsmgr?ref_type=heads).
It defines namespace, deployment, and service, and an Ansible execution container serving as
the webhook target to launch Ansible configuration runs of the DNS and DHCP servers.

## Your Service Layer
A production example can be found in
[my application configuration repo](https://gitlab.com/BenGTO/kube-02/-/tree/main/k8s-apps/dnsmgr).

In this repository, the base template repo has been added as git submodule to the git root. The
local `kustomize.yml` includes the template and adds instance specific details to form a
production setup.

A production setup has to add an ingress, and supply the connection details for the database. The
ingress is instance specific because of the hostname. The patch file `variables.yml` replaces or
adds the connection details to the deployment.

The database credentials are taken from a separate secret, encrypted. For manual deployment, you
can create an usual secret from literals.
