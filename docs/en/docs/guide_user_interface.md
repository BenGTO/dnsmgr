# User Interface

## Global Actions
![Screenshot of the global action buttons](img/global_action_buttons.png)

In the top right corner are the global action buttons. From left to right:

 - [Trigger webhook](guide_initial_config.md#webhook)
 - Search
 - [Main configuration](guide_initial_config.md)
 - Set your profile, [manage your access tokens](guide_tokens.md) and change your password
 - Logout

### Webhook
If setup correctly, this will push the configuration to your DNS and DHCP servers.

### Search
Searches for strings in DNS names, aliases, comments, and MAC addresses.

### Main Configuration
Before you can create configuration files, the main configuration has to be
completed. See more details in the next section.

### User Management
If your user has administrator rights, you'll see the user management symbol.

### User Profile
Here you can change your password and select a dark layout.

### Logout
Nothing to say ...

## Item Listings
In the [network](guide_network.md) and [host](guide_host.md) listings, click on
the name to edit the item. Click on the IP
address to list attached elements: hosts for networks, aliases for hosts.

On the right side are buttons for the same actions, and the delete button.
