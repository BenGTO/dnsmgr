# Configuration
The database connection can be configured with environment variables:

 - `DB_HOST`: set this to the hostname of the database host. If left emtpy,
   a local sqlite database will be used.
 - `DB_TYPE`: type of database server, may be set to `sqlite` (default) or `mysql`.
 - `DB_NAME`: database name
 - `DB_PORT`: port to connect for database access (default 3306)
 - `DB_USER`: database user, default: dnsmgr
 - `DB_PASSWORD`: password for the database user
 - `DB_PATH`: path to the database file if sqlite is used. Defaults to
   `<install_dir>/instance/db`. In Docker containers, you can mount a
   volume to `/app/instance/db` to store the database on a persistent volume.
 - `SECRET_KEY`: See below
 - `RESTRICT_ACCESS_TO_DUMPS`: See below

## Secret Key
The application uses cryptographically signed cookies to track user session. The `SECRET_KEY`
is the base of the cryptographic process, keep it secret, as it's name says.

A good key can be created with the command
```shell
python -c 'import secrets; print(secrets.token_hex())'
```

## Restrict access to dump
By default, the access to the DNS and DHCP configuration dumps is unrestricted. Setting the env
variable `RESTRICT_ACCESS_TO_DUMPS` requires any request to the HTTP endpoints to be authenticated.
Requesting applications can make use of [authorization tokens](guide_tokens.md).
