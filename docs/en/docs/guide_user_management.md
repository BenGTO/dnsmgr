# User Management

You can add users and apply an access role:

- administrator
- user
- readonly

## Readonly
A readonly user has access to most of the information, but may not alter any data.

## User
A user can manage networks, hosts and aliases.

## Administrator
Only administrators have access to the user management. The account initially
created has administrator rights.
