# Initial Configuration
Click on the cog wheel to get to the configuration page.

### Primary DNS Name
If you have primary DNS and replicas, then enter the main DNS name here. If you deploy
the configuration directly to all of your DNS servers, this field has no effect.

### Time to Live
The default TTL for a DNS response.

### DHCP
Most of the settings are obvious. The option _Shared networks_ is necessary, if several
networks share the same wire (no VLANs).

### Webhook
URL of the webhook. This address will be called to launch the configuration run. Enter
a valid URL for a HTTP GET request. It is only a trigger, no data will be submitted.
