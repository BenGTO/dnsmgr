Package Installation
====================

**Warning**: installations from package currently do not perform automatic database upgrades.

## Installation
Create a Python virtual environment:

```shell
python -m venv venv
```

Activate the environment. For Linux:
```shell
cd venv
. venv/bin/activate
```
or Windows:
```shell
venv\Scripts\activate
```
Optionally, update your `pip`:
```shell
pip install -U pip
```
Install _dnsmgr_:
```shell
pip install pip install dnsmgr --index-url https://gitlab.com/api/v4/projects/7786037/packages/pypi/simple
```

## Execution
A test launch using a in memory database with:
```shell
flask -A dnsmgr run
```
should give you this output:

```
 * Serving Flask app 'dnsmgr'
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
```
To be reachable from the network, add this option:
```
flask -A dnsmgr run --host 0.0.0.0
```

The warning about the production environment can be ignored. The builtin Flask server is capable of serving
the application to a single user.

For a secure HTTPS connection, use a proxy like nginx.

## Configuration
The [configuration variables](configuration.md) can be written to a file like `env`:
```shell
export SECRET_KEY=d1be388a461fe861403d2919a5a1dfc0dccd82b737d4dc2ff59d0ff39ba1495c
export DB_PATH=/var/lib/dnsmgr
```
and sourced before the application starts:

```shell
. ./env
flask -A dnsmgr run --host 0.0.0.0
```

## Database Update
Upgrades from a minor release to the next (i.e. 1.5.4 -> 1.6.0) may require updating the database schema.
A database upgrade is idempotent, it can always be applied and only changes things when it is required.
If there is nothing to do, it does nothing.

The path to the files required for the update depends on your virtual environment.
After the steps from above, issue a command like from the
```shell
flask db update -d <path to your venv>/lib/python<your python version>/site-packages/dnsmgr/migrations
```
