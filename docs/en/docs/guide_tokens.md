# Authorization Token

An authorization token is a cryptographically secured piece of data which contains information
about the owning user and the validity of the token.

It is mainly used for authorizing applications to enact as the owning user, without the need to
expose a personal password. The lifetime of the token may be restricted, and a token can be redrawn at any time.

If you have [enforced authentication](configuration.md#restrict-access-to-dump) for the configuration dumps,
you probably want to use a token.

## Token Management
The link to the token management screen can be found in the [user profile settings](guide_user_interface.md).
It lists all of your tokens, with their names, validity states, and description if submitted.

Token may expire, but they are not deleted upon expiration. You have to delete them manually.

### Token Creation
In the _Add Token_ dialog, the lifetime field accepts human-readable time spans:

 - seconds: '30s'
 - minutes: '10m'
 - hours: '5h'
 - days: '1d'
 - weeks: '2w'
 - months: '1month', '2 months'
 - years: '3y'

and combinations like '1d 6h'.

Upon submission, the newly created token will be revealed for the only time, it will not be
displayed later in the management screen.

### Token Usage in Applications
The token has to be submitted as part of the HTTP request header. For _curl_, you would type

```shell
curl -H "Authentication: Bearer eyJh....<token>....." http://example.com
```
