# MariaDB/MySQL

To provide _dnsmgr_ access to a database, you have to create a database named `dnsmgr` first. Log in
as administrator in a mysql shell and type

```
CREATE DATABASE dnsmgr;
```

Create a user with access to this database:

```
CREATE USER 'dnsmgr'@'%' IDENTIFIED BY 'my_secret_password';
GRANT ALL PRIVILEGES ON `dnsmgr`.* TO 'dnsmgr'@'%';
```

If you use an installation based on the Docker image, you are done now. If you use a package based
installation, you should proceed by performing the DB update steps described on the [package
installation page](installation_package.md).
