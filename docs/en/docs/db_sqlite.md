# SQlite

This is the simplest database type. No server is needed, the database is directly written to disk.

The only configuration option is `DB_PATH`, pointing to the database file:

```
DB_PATH=/var/lib/dnsmgr/app.db
```
