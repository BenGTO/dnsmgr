# Add a Network
Click on *Networks* in the breadcrumb bar, then *Add network*.

## Network Information
The name can be chosen freely, it is used in the network listing only. Address,
netmask and gateway are nothing special, i.e.:

 - Name: Backbone
 - Address: 192.168.0.0
 - Netmask: 255.255.255.0
 - Gateway: 192.168.0.1

## Name Servers
For the DHCP server:

 - Name servers: 192.168.0.100,192.168.0.101,8.8.8.8
 - Netbios server: 192.168.0.99

## Address Pool
Define a pool of addresses available for dynamic IP addresses. The options configure the
way how clients are served by this pool (or forced to another pool in another subnet).

## Timing
Control how quickly clients ask for a lease renewal, and how long a lease will be valid.

## Bootserver
Configure the _next server_ option which allows a PXE client to find a bootimage.
