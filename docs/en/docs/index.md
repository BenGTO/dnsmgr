DNS Manager for Home Networks
=============================
Introduction
------------
My first network was a parallel port cable connecting two Amigas. A few years later,
with an ADSL router, the network had grown to four computers. Still, there was no need for
managing a network.

Times have changed since then. Workstations, printer, gaming consoles, mobile devices, IoT devices,
routers and switches, you name it. Add some kind of server with a couple of containers or VMs,
and you end up with a hundred IP clients. Many of them have to be reachable for login or
configuration interfaces.

Enter _dnsmgr_: just enough DNS management for the network at home.

Manage your IP ranges, DNS names, MAC addresses and aliases in a web application. Then dump
ready to use configuration files for _bind_ and/or _ISC dhcpd_.

## Features
- Manages multiple networks
- Lease time, name server, boot server per network
- IP ranges for dynamic IPs
- Search for host names, aliases and MAC addresses
- provides consistent configuration files for
  - ISC dhcp server
  - bind DNS server
- User roles: Administrator, user, read only
- Supports [SQlite](db_sqlite.md), [MySQL/MariaDB](db_mariadb.md), [PostgreSQL](db_postgresql.md)

## Not Features
 - Tenants
 - Multiple domains
